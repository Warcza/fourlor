﻿using UnityEngine;
using UnityEngine.UI;

public class ModeMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Mode;
		}
	}

	[SerializeField] Image ContainerImg;
	[SerializeField] Sprite ValideImg;
	[SerializeField] Sprite BackImg;
	[SerializeField] float timePlacement;

	[Space]
	[Header ("References")]
	[SerializeField] Transform [ ] AllButton;
	[SerializeField] GameObject [ ] LockMode;
	[SerializeField] ContainerDetail [ ] AllContainer;

	Vector3 [ ] getPos;
	Vector2 getRect;
	PlayModeToken setToken;

	infoDate [ ] saveDates;
	infoDate currDate;
	int saveIDLock;

	bool checkContainer;

	struct infoDate
	{
		public int Year;
		public int Month;
		public int Day;
	}
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		checkDate ( );
		Manager.UI.randPos (AllButton, getRect);
		Manager.UI.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.UI.MenuColor);
		Manager.UI.UisPosition (AllButton, getPos, timePlacement, enableButton);
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		ContainerImg.sprite = ValideImg;

		if (checkContainer)
		{
			OpenDetailMenu (true);
		}

		int getLength = AllButton.Length;

		for (int a = 0; a < getLength; a++)
		{
			AllButton [a].localPosition = getPos [a];
		}

		setButton (false, AllButton);
	}

	public void Standard ( )
	{
		if (IsActive)
		{
			IsActive = false;
			launchGame (GameType.Standard);
		}
	}

	public void GodMode ( )
	{
		if (IsActive)
		{
			IsActive = false;
			if (!LockMode [0].activeSelf)
			{
				launchGame (GameType.GodMode);
			}
			else
			{
				launchGame (GameType.GodMode);
			}
		}
	}

	public void WhiteBlack ( )
	{
		if (IsActive)
		{
			IsActive = false;
			if (!LockMode [1].activeSelf)
			{
				launchGame (GameType.BlackWhith);
			}
			else
			{
#if UNITY_ANDROID
				RewardUnlock (1);
#else
				launchGame (GameType.BlackWhith);
#endif
			}
		}
	}

	public void OpenDetailMenu (bool instant = false)
	{
		checkContainer = !checkContainer;

		if (checkContainer)
		{
			ContainerImg.sprite = BackImg;
		}
		else
		{
			ContainerImg.sprite = ValideImg;
		}

		for (int a = 0; a < AllContainer.Length; a++)
		{
			AllContainer [a].UseContainer (checkContainer, instant);
		}
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		setToken = new PlayModeToken ( );
		currDate = new infoDate ( );

		initPosRectButton ( );

		int getLength = LockMode.Length;
		string getA;
		saveDates = new infoDate [getLength];

		for (int a = 0; a < getLength; a++)
		{
			getA = a.ToString ( );

			saveDates [a].Year = AllPlayerPrefs.GetIntValue (getA + Constants._Year, 0);
			saveDates [a].Month = AllPlayerPrefs.GetIntValue (getA + Constants._Month, 0);
			saveDates [a].Day = AllPlayerPrefs.GetIntValue (getA + Constants._Day, 0);
		}

		getLength = AllButton.Length;
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;

		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}

		setButton (false, AllButton);
	}

	void initPosRectButton ( )
	{
		Vector2 getSize = Manager.UI.RectScreenSize * 1.01f;
		AllButton [0].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x * 0.5f, getSize.y * 0.5f);
		AllButton [1].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x * 0.5f, getSize.y * 0.5f);
		AllButton [2].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x, getSize.y * 0.5f);

		AllButton [0].localPosition = new Vector2 (-getSize.x * 0.25f, -getSize.y * 0.25f) * Constants.F_RatioLess;
		AllButton [1].localPosition = new Vector2 (getSize.x * 0.25f, -getSize.y * 0.25f) * Constants.F_RatioLess;
		AllButton [2].localPosition = new Vector2 (0, getSize.y * 0.25f) * Constants.F_RatioLess;

		Manager.UI.UpdateScaleChild (AllButton [0]);
		Manager.UI.UpdateScaleChild (AllButton [1]);
		Manager.UI.UpdateScaleChild (AllButton [2]);
	}

	void launchGame (GameType thisType)
	{
		setToken.ThisMode = thisType;
		IsActive = false;

		Manager.UI.DisplayMenu (MenuType.Play, setToken);
	}

	void enableButton ( )
	{
		Manager.UI.CloseMenu (MenuType.Home);
		setButton (true, AllButton);
	}

	void RewardUnlock (int thisMode)
	{
		IsActive = false;

		saveIDLock = thisMode;
		Manager.ADS.ShowRewardedAd (unlockMode, failUnLock);
	}

	void unlockMode ( )
	{
		AllPlayerPrefs.SetIntValue (Constants._AdCount, 10);

		saveDates [saveIDLock].Year = System.DateTime.Today.Year;
		saveDates [saveIDLock].Month = System.DateTime.Today.Month;
		saveDates [saveIDLock].Day = System.DateTime.Today.AddDays (1).Day;

		AllPlayerPrefs.SetIntValue (saveIDLock + Constants._Year, saveDates [saveIDLock].Year);
		AllPlayerPrefs.SetIntValue (saveIDLock + Constants._Month, saveDates [saveIDLock].Month);
		AllPlayerPrefs.SetIntValue (saveIDLock + Constants._Day, saveDates [saveIDLock].Day);

		LockMode [saveIDLock].SetActive (false);
		waitActive ( );
	}

	void failUnLock ( )
	{
		IsActive = true;
	}

	void checkDate ( )
	{
		currDate.Year = System.DateTime.Today.Year;
		currDate.Month = System.DateTime.Today.Month;
		currDate.Day = System.DateTime.Today.Day;

		int getLength = LockMode.Length;

		for (int a = 0; a < getLength; a++)
		{
			if (saveDates [a].Day > currDate.Day || saveDates [a].Month > currDate.Month || saveDates [a].Year > currDate.Year)
			{
				LockMode [a].SetActive (false);
			}
		}
	}

	async void waitActive ( )
	{
		await System.Threading.Tasks.Task.Delay (50);

		IsActive = true;
	}
	#endregion
}