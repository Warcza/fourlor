﻿using System.Threading;

using UnityEngine;
using UnityEngine.UI;

public class ContainerDetail : MonoBehaviour
{
	#region Variables
	[SerializeField] float timePlacement = 1;

	[Space]
	[SerializeField] DetailScriptable DetailList;
	[SerializeField] ModeMenu parentMenu;
	[SerializeField] GameType ThisType;

	[Space]
	[SerializeField] Slider thisSlider;
	[SerializeField] Sprite Standard;
	[SerializeField] Sprite Perfect;

	[SerializeField] float startBack = 1;

	infoButton [ ] buttonsInfo;
	Transform [ ] allImg;
	Vector3 StartPos;
	Vector3 NewPos;

	CancellationTokenSource tok;
	PlayModeToken setToken;

	float getSize;
	float getSize2;

	int index;
	bool checkContainer = false;

	[System.Serializable]
	struct infoButton
	{
		public Button ThisButton;
		public GameObject LockObj;
		public Image PerfectImg;
		public Text Score;
		public Text NameMusic;

	}
	#endregion

	#region Mono
	void Awake ( )
	{
		setToken = new PlayModeToken ( );

		Transform thisTrans = transform;
		tok = new CancellationTokenSource ( );

		getSize = Manager.UI.RectScreenSize.x * 0.5f;
		getSize2 = GetComponent<RectTransform> ( ).sizeDelta.x * thisTrans.localScale.x;
		int dir = 1;

		if (startBack > 0)
		{
			dir = -1;
		}

		thisTrans.localPosition = new Vector2 (getSize * startBack - getSize2 * 0.5f * dir, 0);

		Transform currButton;
		int length = 3;
		allImg = new Transform [length];
		buttonsInfo = new infoButton [length];

		for (int a = 0; a < length; a++)
		{
			currButton = thisTrans.GetChild (a);
			allImg [a] = currButton;

			buttonsInfo [a].NameMusic = currButton.GetChild (0).GetComponent<Text> ( );
			buttonsInfo [a].Score = currButton.GetChild (1).GetComponent<Text> ( );
			buttonsInfo [a].PerfectImg = currButton.GetChild (2).GetComponent<Image> ( );
			buttonsInfo [a].LockObj = currButton.GetChild (3).gameObject;

			buttonsInfo [a].ThisButton = currButton.GetComponent<Button> ( );
		}

		for (int a = 0; a < length; a++)
		{
			allImg [a] = buttonsInfo [a].ThisButton.transform;
		}

		thisSlider.minValue = 0;
		thisSlider.maxValue = DetailList.AllDetail.Length;

		StartPos = thisTrans.localPosition;
		NewPos = thisTrans.localPosition - new Vector3 (getSize * startBack - getSize2 * 0.5f * dir, 0, 0);
		gameObject.SetActive (false);
	}
	#endregion

	#region Public Methodes
	public void UseContainer (bool enable, bool instant = false)
	{
		index = 0;

		if (enable)
			gameObject.SetActive (true);

		if (tok != null)
		{
			if (tok.Token.CanBeCanceled)
			{
				tok.Cancel ( );
			}
			tok.Dispose ( );
		}

		if (instant && !enable)
		{
			transform.localPosition = StartPos;
			return;
		}

		tok = new CancellationTokenSource ( );

		if (enable)
		{
			setMusic (0);
			gameObject.SetActive (true);
			Manager.UI.ChangeColor (allImg, timePlacement, null, 100, null, Manager.UI.MenuColor, null, tok);
		}

		if (enable)
		{
			Manager.UI.UiPosition (transform, NewPos, timePlacement, null, 100, null, tok);
		}
		else
		{
			if (!instant)
			{
				Manager.UI.UiPosition (transform, StartPos, timePlacement, disableObj, 100, null, tok);
			}
		}
		checkContainer = enable;
	}

	public void NextItem ( )
	{
		if (index + 1 < buttonsInfo.Length && thisSlider.maxValue > 3)
		{
			index++;
			setMusic (index);
		}
	}

	public void PreviousItem ( )
	{
		if (index - 1 > 0 + buttonsInfo.Length)
		{
			index--;
			setMusic (index);
		}
	}

	public void LaunchCustomMusic (int indexButton)
	{
		if (DetailList.AllDetail.Length > index + indexButton && DetailList.AllDetail [index + indexButton].UnLock)
		{
			DetailList.CurrIndex = index;
			AllDetail currDetail = DetailList.AllDetail [index + indexButton];

			setToken.ThisMode = ThisType;
			setToken.CustomMusique = true;

			setToken.MusicName = currDetail.AudioName;
			setToken.CategorieName = currDetail.CategorieName;
			setToken.Level = currDetail.Lvl;

			Manager.UI.DisplayMenu (MenuType.Play, setToken);
		}
	}

	#endregion

	#region Private Methodes
	void setMusic (int index)
	{
		AllDetail [ ] currDetail = DetailList.AllDetail;
		thisSlider.value = index + buttonsInfo.Length;

		for (int a = 0; a < buttonsInfo.Length; a++)
		{
			if (index + a >= currDetail.Length)
			{
				buttonsInfo [a].NameMusic.text = string.Empty;
				buttonsInfo [a].Score.text = string.Empty;
				buttonsInfo [a].PerfectImg.enabled = false;
				buttonsInfo [a].LockObj.SetActive (false);
				continue;
			}

			buttonsInfo [a].PerfectImg.enabled = true;
			buttonsInfo [a].NameMusic.text = currDetail [a].AudioName;
			buttonsInfo [a].Score.text = currDetail [a].Score.ToString ( );
			buttonsInfo [a].LockObj.SetActive (!currDetail [a].UnLock);
			if (currDetail [a].Perfect)
			{
				buttonsInfo [a].PerfectImg.sprite = Perfect;
			}
			else
			{
				buttonsInfo [a].PerfectImg.sprite = Standard;
			}
		}
	}

	void disableObj ( )
	{
		gameObject.SetActive (false);
		if (checkContainer)
		{
			checkContainer = false;
			transform.localPosition = StartPos;
		}
	}
	#endregion
}