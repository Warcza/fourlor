﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;

using UnityEngine;
using UnityEngine.UI;

public class BonusMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Bonus;
		}
	}

	[SerializeField] float timePlacement;
	[SerializeField] Transform [ ] AllButton;
	[SerializeField] Transform BackButton;

	Vector3 [ ] getPos;
	Vector2 getRect;

	AudioClip saveClip;

	bool checkClip;
	bool checkFile;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		Manager.UI.randPos (AllButton, getRect);
		Manager.UI.UisPosition (AllButton, getPos, timePlacement, enableButton);
		checkClip = false;
		checkFile = false;
		if (GetTok != null)
		{
			MenuToken getMenuTok = (MenuToken)GetTok;
			Manager.UI.ChangeColor (AllButton, timePlacement, getMenuTok.ThisAction, 100, null, Manager.UI.MenuColor);
			Manager.Garbage.AddGarbageClass (getMenuTok);
		}
		else
		{
			Manager.UI.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.UI.MenuColor);
		}
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		setButton (false, AllButton);
	}

	public void BackHome ( )
	{
		MenuToken thisTok = new MenuToken ( );
		thisTok.ThisAction = closeThisMenu;
		Manager.UI.DisplayMenu (MenuType.Home, thisTok);
	}

	public void openFile ( )
	{
		string path = Crosstales.FB.FileBrowser.OpenSingleFile ("Open File", string.Empty, "ogg");
		StartCoroutine (LoadAudioFile (path));
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;

		initPosRectButton ( );

		int getLength = AllButton.Length;
		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}

		setButton (false, AllButton);
	}

	void initPosRectButton ( )
	{
		Vector2 currRect = Manager.UI.RectScreenSize;
		for (int a = 0; a < 3; a++)
		{
			Manager.UI.UpdateScaleChild (AllButton [a]);
		}

		AllButton [0].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y);
		AllButton [1].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f + 1);
		AllButton [2].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f + 1);

		currRect *= 0.5f;
		AllButton [0].localPosition = new Vector2 (-currRect.x * 0.5f, 0) * Constants.F_RatioLess;
		AllButton [1].localPosition = new Vector2 (currRect.x * 0.5f, -currRect.y * 0.5f) * Constants.F_RatioLess;
		AllButton [2].localPosition = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f) * Constants.F_RatioLess;

		Vector2 sizeButton = BackButton.GetComponent<RectTransform> ( ).sizeDelta * 0.5f * BackButton.localScale.x;
		BackButton.localPosition = new Vector2 (currRect.x * 0.5f, -currRect.y * 0.5f) * 0.9f + new Vector2 (-sizeButton.x, sizeButton.y);
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;
	}

	void rebuildList (string e, Text thisTxt)
	{
		for (int a = 0; a < e.Length; a++)
		{
			thisTxt.text += e [a].ToString ( );
		}
	}

	void enableButton ( )
	{
		Manager.UI.CloseMenu (MenuType.Home);
		setButton (true, AllButton);
	}

	void closeThisMenu ( )
	{
		Manager.UI.CloseMenu (MenuType.Bonus);
	}

	IEnumerator LoadAudioFile (string path)
	{
		WWW data = new WWW (path);
		yield return data;

		AudioClip ac = data.GetAudioClip (false, false, UnityEngine.AudioType.OGGVORBIS)as AudioClip;
		if (ac != null)
		{
			for (int a = path.Length - 1; a > 0; a--)
			{
				if (path [a].ToString ( ) == "\\")
				{
					break;
				}
				ac.name += path [a];
			}
			ac.name = "test.mp3";

			saveClip = ac;

			path = Crosstales.FB.FileBrowser.OpenSingleFile ("Open File", string.Empty, "txt");
			StartCoroutine (LoadTextFile (path));
		}
	}

	IEnumerator LoadTextFile (string path)
	{
		WWW data = new WWW (path);
		yield return data;

		if (path.Length > 8 && path.Substring (path.Length - 8) == ".mid.txt")
		{
			PlayModeToken thisTok = new PlayModeToken ( );
			thisTok.ThisClip = saveClip;
			thisTok.CustomMusique = true;
			thisTok.CustomPath = true;
			thisTok.Level = 0;
			thisTok.MusicName = path.Substring (0, path.Length - 4);
			thisTok.ThisMode = GameType.Standard;
			thisTok.data = data.bytes;

			Manager.UI.DisplayMenu (MenuType.Play, thisTok);
		}
	}
	#endregion
}