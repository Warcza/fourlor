﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Play;
		}
	}

	[SerializeField] bool autoValidate;

	[Header ("Placement")]
	[SerializeField] float SpeedLine;
	[SerializeField] float SpeedTarget;
	[SerializeField] float timePlacement;
	[SerializeField] [Range (0, 100)] float PourcWaitLine;

	[Space]
	[Header ("References")]
	[SerializeField] Transform [ ] LinePos;
	[SerializeField] Transform [ ] TargetPos;
	[SerializeField] AnimationCurve CurveTarget;

	[SerializeField] Transform Target;
	[SerializeField] Transform PosLineStart;
	[SerializeField] Transform parentObs;
	[SerializeField] GameObject prefabParticle;
	[SerializeField] GameObject prefabObs;

	[Space]
	[SerializeField] float timeTargetSize;
	[SerializeField] [Range (0.1f, 1)] float SizePourcentage;
	[SerializeField] [Range (0.1f, 2)] float SizePourcentageButton;
	[Space]

	[Header ("KeyCode")]
	[SerializeField] keyCodeInfo [ ] allKeyCode;

	List<infoParticle> garbageParticle;
	List<GameObject> otherGarbagePart;
	List<infoObs> garbageObs;
	List<infoObs> allObj;
	List<string> catNameMusic;
	List<int> listScore;

	List<Transform> removeLine;
	List<LineController> currentLines;

	infoObs lastSpawn;

	CustomMusic currCustoMusic;
	LevelInfo currLI;

	CancellationTokenSource thisTok;
	MenuTokenAbstract GetLastTok;
	GameOverToken overTok;
	AudioToken thisAudio;

	WaitForSeconds waitStart;
	WaitForSeconds waitSec;
	WaitForEndOfFrame waitF;

	AudioClip saveCustomClip;
	GameType gameMode;
	Camera cam;

	float timeDest;
	float minDistObj;

	int waveLvlUp;
	int currWave;
	int currLvl;

	int currNbrElem;
	int countNbrElem;
	int lastLine;

	int posTarget;
	int currMove;

	int nbrToSpawn;
	int checkRdy;

	string getMusic;
	string getCatName;

	bool checkShortDist;
	bool checkLastTarget;
	bool checkGameOver;
	bool checkPerfect;
	bool gameOver;
	bool customMusic;

	byte [ ] data;

	struct CustomMusic
	{
		public LevelInfo ThisLevelInfo;
		public string CategorieName;
		public string MusicName;
	}

	struct infoObs
	{
		public GameObject ThisObj;
		public ObsData ThisData;
		public Vector3 StartPos;
	}

	[System.Serializable]
	struct infoParticle
	{
		public GameObject ThisObj;
		public ParticleSystem ThisParticle;
	}

	[System.Serializable]
	struct keyCodeInfo
	{
		public KeyCode thisKey;
		public UnityEvent MoveDirection;
	}
	#endregion

	#region Mono
	void Update ( )
	{
		if (!IsActive)
		{
			return;
		}

		/*while (autoValidate && allObj.Count > 0 && allObj [0].ThisData.CheckDetect && Vector2.Distance ((Vector2)allObj [0].ThisData.Destination, (Vector2)allObj [0].ThisObj.transform.localPosition) < 10)
		{
			//UseButton (allObj [0].BtnImg);
		}*/

		int length = allKeyCode.Length;
		float sizePourc = Manager.UI.RatioScreen.x;
		for (int a = 0; a < length; a++)
		{
			if (Input.GetKeyDown (allKeyCode [a].thisKey))
			{
				allKeyCode [a].MoveDirection.Invoke ( );
				//UseButton (img.buttonImg);
			}
		}

		if (gameOver && !checkGameOver)
		{
			checkGameOver = true;
			stopGame ( );
			return;
		}

		if (nbrToSpawn > 0)
		{
			spawnObs ( );
			nbrToSpawn--;
		}
	}
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		checkLastTarget = false;

		checkPerfect = true;

		launchGame (GetTok);

		posTarget = (int)(TargetPos.Length * 0.5f) + 1;
		currMove = 0;

		Target.localPosition = TargetPos [posTarget].localPosition;
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		int length = removeLine.Count;
		for (int a = 0; a < length; a++)
		{
			removeLine [a].localPosition = PosLineStart.localPosition;
			Manager.Game.StockLine (removeLine [a].gameObject);
		}

		removeLine.Clear ( );
	}

	public void UseButton (Image thisImg)
	{
		if (IsActive && allObj.Count > 0)
		{
			infoObs currInfo = allObj [0];
			if (currInfo.ThisData.CheckDetect)
			{
				checkScore (Vector2.Distance (currInfo.ThisObj.transform.localPosition, currInfo.ThisData.Destination));
				currInfo.ThisObj.SetActive (false);
				currInfo.ThisObj.transform.SetParent (parentObs.parent);
				garbageObs.Add (currInfo);
				allObj.RemoveAt (0);
			}
			else
			{
				checkPerfect = false;

				Manager.Game.gameOver ( );
			}

			currInfo.ThisData.ResetValue ( );
		}
	}

	public void MoveTarget (bool goRight)
	{
		bool checkBack = false;
		if (goRight)
		{
			if (currMove < 0)
			{
				currMove = 0;
				checkBack = true;
			}
			currMove++;
		}
		else
		{
			if (currMove > 0)
			{
				currMove = 0;
				checkBack = true;
			}
			currMove--;
		}
		movingTarget (checkBack);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		garbageParticle = new List<infoParticle> ( );
		currentLines = new List<LineController> ( );
		removeLine = new List<Transform> ( );
		otherGarbagePart = new List<GameObject> ( );
		catNameMusic = new List<string> ( );
		garbageObs = new List<infoObs> ( );
		listScore = new List<int> ( );
		allObj = new List<infoObs> ( );

		currCustoMusic = new CustomMusic ( );
		thisAudio = new AudioToken ( );
		waitSec = new WaitForSeconds (1.5f);
		waitStart = new WaitForSeconds (timePlacement * (PourcWaitLine / 100));
		overTok = new GameOverToken ( );

		thisAudio.ThisType = AudioType.MBG;
		thisAudio.MidiActSpawn = newSpawn;
		thisAudio.LoopAudio = false;
		thisAudio.AudioAct = AudioAction.Renew;

		gameMode = GameType.Standard;
		cam = Manager.UI.GetCam;

		initPosRectButton ( );

		System.Action<GameTypeEvent> thisAction = delegate (GameTypeEvent thisEvnt)
		{
			gameOver = true;
		};

		Manager.Event.Register (thisAction);
	}

	void initPosRectButton ( )
	{
		float getRatio = Manager.UI.RatioScreen.x;
	}

	void startGame ( )
	{
		Manager.UI.CloseMenu (1);
		nextLvl ( );
	}

	void waitNextMusic ( )
	{
		StartCoroutine (waitRdy ( ));
	}

	void autoMove ( )
	{
		movingTarget ( );
	}

	void movingTarget (bool Back = false)
	{
		int lastPos = posTarget;
		if (currMove > 0)
		{
			currMove--;
			posTarget++;
		}
		else if (currMove < 0)
		{
			currMove++;
			posTarget--;
		}
		else
		{
			return;
		}

		if (Back)
		{
			if (thisTok != null)
			{
				if (thisTok.Token.CanBeCanceled)
				{
					thisTok.Cancel ( );
				}
				thisTok.Dispose ( );
			}
		}

		if (posTarget < 0 || posTarget > TargetPos.Length - 1)
		{
			posTarget = lastPos;
		}
		else
		{

			thisTok = new CancellationTokenSource ( );
			float dist = Vector3.Distance (TargetPos [0].localPosition, TargetPos [1].localPosition);
			float getTime = (dist * (Vector3.Distance (TargetPos [posTarget].localPosition, Target.localPosition) / dist)) / SpeedTarget;
			Manager.UI.UiPosition (Target, TargetPos [posTarget].localPosition, getTime, autoMove, 100, CurveTarget, thisTok);
		}
	}

	void launchGame (MenuTokenAbstract GetTok = null)
	{
		PlayModeToken thisTok = (PlayModeToken)GetTok;
		gameMode = thisTok.ThisMode;
		overTok.playToke = thisTok;
		customMusic = thisTok.CustomMusique;
		saveCustomClip = thisTok.ThisClip;
		currCustoMusic.CategorieName = thisTok.CategorieName;
		currCustoMusic.MusicName = thisTok.MusicName;
		thisAudio.CustomPath = thisTok.CustomPath;

		overTok.searchData = !thisTok.CustomPath;
		data = thisTok.data;

		currLvl = 0;
		currWave = 0;
		nbrToSpawn = 0;

		gameOver = false;
		checkGameOver = false;

		startGame ( );
		if (customMusic)
		{
			getMusic = thisTok.MusicName;
			getCatName = thisTok.CategorieName;

			thisAudio.ThisAct = Manager.Game.gameOver;
		}
		else
		{
			thisAudio.ThisAct = waitNextMusic;
		}
	}

	void setSpawner (int nbrSpawner)
	{
		checkLine (nbrSpawner);
		if (checkRdy == 0)
		{
			checkRdy--;

			checkUpdateRdy ( );
		}
	}

	#region Check
	void checkLine (int nbrSpawner)
	{
		if (currentLines.Count < nbrSpawner)
		{
			GameObject newObj;
			Vector3 startPos = PosLineStart.localPosition;
			Vector3 dir = (startPos - LinePos [0].transform.localPosition);
			float dist = dir.magnitude;

			dir = dir.normalized;

			int count = 0;
			while (currentLines.Count < nbrSpawner)
			{
				newObj = Manager.Game.GetRandomLine ( );
				newObj.transform.SetParent (parentObs);
				newObj.transform.localScale = Vector3.one;
				newObj.transform.localPosition = startPos - dir * count * dist;
				currentLines.Add (newObj.GetComponent<LineController> ( ));
				count++;
			}

			Transform transLine;
			Vector3 posLine;
			int bonus = (int)(nbrSpawner * 0.5f) + 1;
			for (int a = 0; a < nbrSpawner; a++)
			{
				transLine = currentLines [a].transform;
				posLine = LinePos [a + bonus].localPosition;
				transLine.GetComponent<LineController> ( ).Line = a + bonus;

				checkRdy--;
				Manager.UI.UiPosition (transLine, posLine, Vector3.Distance (transLine.localPosition, posLine) / SpeedLine, checkUpdateRdy);
			}
		}
		else
		{
			checkRdy--;
			Transform transLine;
			Vector3 posStart = PosLineStart.position;
			while (currentLines.Count > nbrSpawner)
			{
				transLine = currentLines [currentLines.Count - 1].transform;
				removeLine.Add (transLine);
				currentLines.RemoveAt (currentLines.Count - 1);

				Manager.UI.UiPosition (removeLine [removeLine.Count - 1], posStart, Vector3.Distance (transLine.position, posStart) / SpeedLine, checkUpdateRdy);
			}
		}
	}

	void checkScore (float distance)
	{
		distance = (distance / 100) * 100;

		int startValue = 100;
		int currScore = -1;
		do
		{
			startValue -= 25;
			currScore++;

		} while (distance < startValue);

		listScore.Add (currScore);

		if (checkPerfect)
		{
			checkPerfect = currScore == 3;
		}

		while (currScore < 3)
		{
			currScore++;
		}
	}

	void checkUpdateRdy ( )
	{
		checkRdy++;

		if (checkRdy == 0)
		{
			StartCoroutine (waitRdy ( ));
		}
	}
	#endregion

	void nextLvl ( )
	{
		currWave = 0;
		catNameMusic.Clear ( );

		LvlInfo getLvl;

		if (!customMusic)
		{
			currLI = Manager.Game.GetLevel (currLvl, gameMode);
		}
		else
		{
			currLI = Manager.Game.GetLevel (overTok.playToke.Level, gameMode, overTok.playToke.CategorieName);
		}

		getLvl = currLI.InfoLevel;
		currLvl++;

		timeDest = getLvl.TimeToDestination;
		waveLvlUp = getLvl.WaveLvlUP;

		checkShortDist = getLvl.SameLineShortSpace;
		currNbrElem = getLvl.MaxSameLine;
		minDistObj = getLvl.Dist;

		setSpawner (getLvl.NbrSpawner);
	}

	void nextMusic ( )
	{
		if (!IsActive)
		{
			return;
		}

		int length = allObj.Count;

		for (int a = 0; a < length; a++)
		{
			allObj [a].ThisObj.SetActive (false);
			allObj [a].ThisObj.transform.SetParent (parentObs.parent);
			garbageObs.Add (allObj [a]);
		}
		allObj.Clear ( );

		if (currWave >= waveLvlUp)
		{
			nextLvl ( );
			return;
		}
		currWave++;

		if (!customMusic)
		{
			int getIndex = Random.Range (0, currLI.CategorieName.Length);

			getCatName = currLI.CategorieName [getIndex];
			thisAudio.CategorieName = getCatName;

			getMusic = Manager.Aud.GetRandomMusic (thisAudio, catNameMusic.ToArray ( ));
		}

		thisAudio.CategorieName = getCatName;
		thisAudio.ThisName = getMusic;
		thisAudio.WaitStart = timeDest;

		overTok.playToke.MusicName = getMusic;
		overTok.playToke.CategorieName = getCatName;

		if (thisAudio.CustomPath)
		{
			Manager.Aud.PlayerCustom (thisAudio, saveCustomClip, data);
		}
		else
		{
			Manager.Aud.OpenAudio (thisAudio);
		}
	}

	void spawnObs ( )
	{
		infoObs getObj;
		if (garbageObs.Count > 0)
		{
			getObj = garbageObs [garbageObs.Count - 1];
			garbageObs.RemoveAt (garbageObs.Count - 1);
			getObj.ThisObj.transform.SetParent (parentObs);
		}
		else
		{
			getObj = new infoObs ( );
			GameObject getNewObj = (GameObject)Instantiate (prefabObs, parentObs);
			getObj.ThisObj = getNewObj;
			getObj.ThisData = getNewObj.GetComponent<ObsData> ( );
		}

		getObj.ThisObj.transform.SetAsFirstSibling ( );

		if (checkLastTarget && (!checkShortDist || countNbrElem > currNbrElem || Vector3.Distance (currentLines [lastLine].pos, lastSpawn.ThisObj.transform.localPosition) < minDistObj))
		{
			lastLine = Random.Range (0, currentLines.Count);
			countNbrElem = 0;
		}
		else
		{
			countNbrElem++;
		}
		checkLastTarget = true;

		currentLines [lastLine].SpawnThisObj (getObj.ThisData, timeDest);
		//getObj.ThisData.CalculMoveSpeed (timeDest, Vector3.Distance (Vector3.zero, getPos) + Vector3.Distance (Vector3.zero, posTarget), getPos, posTarget);

		//getObj.ThisObj.transform.localPosition = getPos;
		getObj.ThisObj.SetActive (true);

		lastSpawn = getObj;
		allObj.Add (getObj);
	}

	void stopGame ( )
	{
		int length = allObj.Count;

		for (int a = 0; a < length; a++)
		{
			allObj [a].ThisObj.SetActive (false);
			allObj [a].ThisObj.transform.SetParent (parentObs.parent);

			garbageObs.Add (allObj [a]);
		}

		length = currentLines.Count;
		while (currentLines.Count > 0)
		{
			removeLine.Add (currentLines [currentLines.Count - 1].transform);
			currentLines.RemoveAt (currentLines.Count - 1);
		}

		infoParticle currParticle;

		length = otherGarbagePart.Count;
		for (int a = length - 1; a > 0; a--)
		{
			currParticle = new infoParticle ( );
			currParticle.ThisObj = otherGarbagePart [a];
			currParticle.ThisParticle = otherGarbagePart [a].GetComponent<ParticleSystem> ( );

			garbageParticle.Add (currParticle);
		}

		otherGarbagePart.Clear ( );

		overTok.AllScore = listScore.ToArray ( );
		overTok.Perfect = checkPerfect;

		allObj.Clear ( );
		listScore.Clear ( );
		catNameMusic.Clear ( );

		Manager.UI.DisplayMenu (MenuType.Over, overTok);
	}

	#region Other Operation
	void newSpawn ( )
	{
		nbrToSpawn++;
	}

	float randomValue (float thisValue)
	{
		if (Random.Range (0, 2) == 0) // Horizontal
		{
			return thisValue;
		}
		else
		{
			return -thisValue;
		}
	}

	IEnumerator waitParticleGarbage (float time, GameObject thisObj)
	{
		while (time > 0)
		{
			time -= Time.deltaTime;
			yield return waitF;
		}

		infoParticle newPart = new infoParticle ( );
		newPart.ThisParticle = thisObj.GetComponent<ParticleSystem> ( );
		newPart.ThisObj = thisObj;

		otherGarbagePart.Remove (thisObj);

		garbageParticle.Add (newPart);
	}

	IEnumerator waitRdy ( )
	{
		if (currWave > 0)
		{
			yield return waitSec;
		}
		else
		{
			yield return null;
		}

		nextMusic ( );
	}
	#endregion
	#endregion
}