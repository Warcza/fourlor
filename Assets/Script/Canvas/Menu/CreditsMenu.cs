﻿using UnityEngine;
using UnityEngine.UI;

public class CreditsMenu : UiParent
{
    #region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Credit;
		}
	}

	[SerializeField] float timePlacement;
	
	[SerializeField] Transform TextTransform;
	[SerializeField] Transform BackButton;

	Vector3 getPos;
	Vector2 getRect;
    #endregion

    #region Mono

    #endregion

    #region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis(GetTok);

		Manager.UI.randPos(TextTransform, getRect);
		Manager.UI.UiPosition(TextTransform, getPos, timePlacement, enableButton);
		Manager.UI.ChangeColor(TextTransform, timePlacement, null, 100, null, Manager.UI.MenuColor);
	}

	public override void CloseThis ()
	{
		base.CloseThis();
		
		setButton(false, BackButton);
	}

	public void BackHome()
	{
		if(IsActive)
		{
			IsActive = false;
			
			MenuToken getTok = Manager.Garbage.GetGarbaClass<MenuToken>(typeof(MenuToken));
			getTok.ThisAction = CloseThis;
			Manager.UI.DisplayMenu(MenuType.Home, getTok);
		}
	}
    #endregion

    #region Private Methodes
	protected override void InitializeUi ( )
	{
		TextTransform.GetComponent<RectTransform>().sizeDelta = Manager.UI.RectScreenSize;
		Manager.UI.UpdateScaleChild(TextTransform);
		getPos = Vector3.zero;
		getRect = Manager.UI.RectScreenSize;
		
		setButton(false, BackButton);
	}

	void enableButton()
	{
		Manager.UI.CloseMenu(MenuType.Home);
		setButton(true, BackButton);
	}
    #endregion
}
