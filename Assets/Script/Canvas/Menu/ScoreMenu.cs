﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Score;
		}
	}

	[SerializeField] float timePlacement;

	[SerializeField] Transform [ ] AllButton;
	[SerializeField] Text [ ] ScoreType;

	Vector3 [ ] getPos;
	Vector2 getRect;

	MenuToken thisMenu;

	bool checkEnable;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		ScoreType [0].text = AllPlayerPrefs.GetIntValue (GameType.Standard.ToString ( ), 0).ToString ( );
		ScoreType [1].text = AllPlayerPrefs.GetIntValue (GameType.GodMode.ToString ( ), 0).ToString ( );
		ScoreType [2].text = AllPlayerPrefs.GetIntValue (GameType.BlackWhith.ToString ( ), 0).ToString ( );

		checkEnable = true;
		Manager.UI.randPos (AllButton, getRect, true, true);
		Manager.UI.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.UI.MenuColor);
		Manager.UI.UisPosition (AllButton, getPos, timePlacement, enableButton);
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );
	}

	public void BackHome ( )
	{
		if (checkEnable)
		{
			checkEnable = false;
			Manager.UI.DisplayMenu (MenuType.Home, thisMenu);
		}
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		thisMenu = new MenuToken ( );
		thisMenu.ThisAction = CloseThis;

		initPosRectButton ( );
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;
		getRect = new Vector2 (getRect.x, 0);

		int getLength = AllButton.Length;
		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}
	}

	void initPosRectButton ( )
	{
		Vector2 getRect = new Vector2 (Manager.UI.RectScreenSize.x, Manager.UI.RectScreenSize.y / 2.9f);
		for (int a = 0; a < 3; a++)
		{
			AllButton [a].GetComponent<RectTransform> ( ).sizeDelta = getRect;
			AllButton [a].localPosition = new Vector2 (0, getRect.y * (a - 1)) * 0.99f;
			Manager.UI.UpdateScaleChild (AllButton [a]);

			AllButton [a].GetChild (0).localPosition = new Vector2 (-getRect.x * 0.25f, 0);
			AllButton [a].GetChild (1).localPosition = new Vector2 (getRect.x * 0.25f, 0);
		}

		Transform backButton = AllButton [0].GetChild (2);
		backButton.localPosition = new Vector2 (-getRect.x * 0.5f + backButton.GetComponent<RectTransform> ( ).sizeDelta.x * backButton.localScale.x, 0);
	}

	void enableButton ( )
	{
		checkEnable = true;
		Manager.UI.CloseMenu (MenuType.Home);
	}
	#endregion
}