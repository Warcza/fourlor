﻿using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class GameManger : ManagerParent
{
	#region Variables
	public LevelInfo [ ] AllLevel;
	[SerializeField] GameObject [ ] AllLine;
	[SerializeField] LevelInfo defaultLevel;
	Dictionary<GameType, Dictionary<int, List<LevelInfo>>> orderScriptable;
	Dictionary<GameObject, List<GameObject>> lineStock;
	List<LevelInfo> listScriptable;

	List<LevelInfo> saveLastScript;
	int lastLvl = -1;

	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public LevelInfo GetLevel (int thisLvl, GameType thisType, string catName = "")
	{
		List<LevelInfo> getList;

		Dictionary<int, List<LevelInfo>> leveltype;
		if (orderScriptable.TryGetValue (thisType, out leveltype))
		{
			if (leveltype.TryGetValue (thisLvl, out getList))
			{
				listScriptable = getList;
			}
			else
			{
				getList = listScriptable;
			}
		}
		else
		{
			getList = listScriptable;
		}

		if (catName == "")
		{
			if (thisLvl != lastLvl)
			{
				thisLvl = lastLvl;
				saveLastScript.Clear ( );
				saveLastScript.AddRange (getList);
			}
			else if (saveLastScript.Count == 0)
			{
				saveLastScript.AddRange (getList);
			}

			int getIndex = Random.Range (0, saveLastScript.Count - 1);
			LevelInfo getLvl = saveLastScript [getIndex];
			saveLastScript.RemoveAt (getIndex);

			return getLvl;
		}
		else
		{
			if (getList == null || getList.Count == 0)
			{
				return defaultLevel;
			}
			LevelInfo currLvl = getList [0];

			int length = getList.Count;
			int lengthB;

			bool checkCurrLvl = false;

			for (int a = 0; a < length; a++)
			{
				lengthB = getList [a].CategorieName.Length;
				for (int b = 0; b < lengthB; b++)
				{
					if (getList [a].CategorieName [b] == catName)
					{
						if (!checkCurrLvl || currLvl.LvlScript > getList [a].LvlScript)
						{
							checkCurrLvl = true;
							currLvl = getList [a];
						}
						break;
					}
				}
			}

			return currLvl;
		}
	}

	public GameObject GetRandomLine ( )
	{
		List<GameObject> lineList;
		GameObject getLine = AllLine [Random.Range (0, AllLine.Length - 1)];

		if (lineStock.TryGetValue (getLine, out lineList))
		{
			GameObject newLine;
			if (lineList.Count > 0)
			{
				newLine = lineList [lineList.Count - 1];
				newLine.SetActive (true);

				lineList.RemoveAt (lineList.Count - 1);
			}
			else
			{
				newLine = (GameObject)Instantiate (getLine);
				newLine.GetComponent<LineController> ( ).PrefabObj = getLine;
			}

			return newLine;
		}

		return null;
	}

	public void StockLine (GameObject thisLine)
	{
		thisLine.SetActive (false);
		GameObject prefObj = thisLine.GetComponent<LineController> ( ).PrefabObj;
		List<GameObject> lineList;
		if (lineStock.TryGetValue (prefObj, out lineList))
		{
			lineList.Add (thisLine);
		}
	}

	public void gameOver ( )
	{
		Manager.Aud.CloseAudio (AudioType.MBG);
		Manager.Aud.stopMid ( );

		GameTypeEvent gameEvent = Manager.Garbage.GetGarbaClass<GameTypeEvent> (typeof (GameTypeEvent));
		if (gameEvent == null)
		{
			gameEvent = new GameTypeEvent ( );
		}
		gameEvent.Raise ( );
		Manager.Garbage.AddGarbageClass (gameEvent);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{

#if UNITY_EDITOR
		Object [ ] getAssets = Resources.LoadAll ("Scriptable/LevelEdit", typeof (LevelScriptable));
		Object [ ] getAssetsAudio = Resources.LoadAll ("Scriptable/", typeof (AudioScriptable));

		LevelScriptable thisLevelScript;
		List<LevelInfo> getAllLvl = new List<LevelInfo> ( );
		List<string> currCat;

		for (int a = 0; a < getAssets.Length; a++)
		{
			thisLevelScript = (getAssets [a] as LevelScriptable);
			currCat = new List<string> ( );

			for (int b = 0; b < getAssetsAudio.Length; b++)
			{
				AudioScriptable thisAud = (AudioScriptable)getAssetsAudio [b];

				for (int c = 0; c < thisLevelScript.ThisLevel.LvlList.Length; c++)
				{
					if (thisAud.ThisMusic.Level == thisLevelScript.ThisLevel.LvlList [c])
					{
						currCat.Add (thisAud.ThisMusic.CategorieName);
						break;
					}
				}
			}

			thisLevelScript.ThisLevel.CategorieName = currCat.ToArray ( );
			getAllLvl.Add (thisLevelScript.ThisLevel);
		}

		AllLevel = getAllLvl.ToArray ( );
#endif

		orderScriptable = new Dictionary<GameType, Dictionary<int, List<LevelInfo>>> ( );
		lineStock = new Dictionary<GameObject, List<GameObject>> ( );
		saveLastScript = new List<LevelInfo> ( );

		int length = AllLine.Length;
		for (int a = 0; a < length; a++)
		{
			lineStock.Add (AllLine [a], new List<GameObject> ( ));
		}

		Dictionary<int, List<LevelInfo>> leveltype;
		List<LevelInfo> getList;
		MusicFX [ ] allMF = Manager.Aud.AllMF;
		length = allMF.Length;

		for (int a = 0; a < length; a++)
		{
			if (allMF [a].ThisGameType == GameType.Nothing)
			{
				continue;
			}

			if (!orderScriptable.TryGetValue (allMF [a].ThisGameType, out leveltype))
			{
				leveltype = new Dictionary<int, List<LevelInfo>> ( );
				orderScriptable.Add (allMF [a].ThisGameType, leveltype);
			}

			int getLengthB = AllLevel.Length;
			for (int b = 0; b < getLengthB; b++)
			{
				if (AllLevel [b].ThisType != allMF [a].ThisGameType)
				{
					continue;
				}

				if (leveltype.TryGetValue (AllLevel [b].LvlScript, out getList))
				{
					getList.Add (AllLevel [b]);
				}
				else
				{
					getList = new List<LevelInfo> ( );
					getList.Add (AllLevel [b]);
					leveltype.Add (AllLevel [b].LvlScript, getList);
				}
			}
		}
	}
	#endregion
}