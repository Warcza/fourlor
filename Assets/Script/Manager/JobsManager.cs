﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

using UnityEngine;
using UnityEngine.Jobs;

public class JobsManager : ManagerParent
{
	#region Variables
	NativeArray<Vector3> thisArray;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public NativeArray<Vector3> IniVectorArray (Vector3 MinValue, Vector3 MaxValue, RandomType RandType, int Lenght)
	{
		if (thisArray.IsCreated)
		{
			thisArray.Dispose ( );
		}
		thisArray = new NativeArray<Vector3> (Lenght, Allocator.Temp);

		for (int a = 0; a < Lenght; a++)
		{
			switch (RandType)
			{
				case RandomType.Standard:
					thisArray [a] = new Vector3 (Random.Range (MinValue.x, MaxValue.x), Random.Range (MinValue.y, MaxValue.y), Random.Range (MinValue.z, MaxValue.z));
					break;
				case RandomType.Clamp:
					if (Random.Range (0, 2) == 0)
					{
						thisArray [a] = MinValue;
					}
					else
					{
						thisArray [a] = MaxValue;
					}
					break;
				case RandomType.Nothing:
					thisArray [a] = MaxValue;
					break;
			}
		}
		return thisArray;
	}

	public void DisposeVector3 ( )
	{
		thisArray.Dispose ( );
	}
	#endregion

	#region Private Methodes
	void OnApplicationQuit ( )
	{
		if (thisArray.IsCreated)
		{
			thisArray.Dispose ( );
		}
	}
	protected override void InitializeManager ( )
	{ }
	#endregion
}

#region JobRegion
public struct J_RotateAround : IJobParallelForTransform
{
	public NativeArray<Vector3> ValueFollow;

	[ReadOnly] public NativeArray<Vector3> DistFollow;
	[ReadOnly] public NativeArray<Vector3> MoveSpeed;

	public Vector3 TargetCenter;
	public float DeltaTime;
	public bool AutoMove;

	public void Execute (int ID, TransformAccess ThisTrans)
	{
		Vector3 getVect = MathFunctions.CubeFollow (ValueFollow [ID], DistFollow [ID]);

		if (AutoMove)
		{
			ValueFollow [ID] += new Vector3 (MoveSpeed [ID].x, MoveSpeed [ID].y, MoveSpeed [ID].z) * DeltaTime;
		}

		ThisTrans.position = Vector3.Lerp (ThisTrans.position, getVect + TargetCenter, DeltaTime);
	}
}

public struct J_MoveTransform : IJobParallelForTransform
{
	public Vector3 TargetPos;

	[ReadOnly] public NativeArray<Vector3> GapPos;
	[ReadOnly] public NativeArray<float> MoveSpeed;

	public float DeltaTime;
	public void Execute (int ID, TransformAccess ThisTrans)
	{
		ThisTrans.position = Vector3.Lerp (ThisTrans.position, TargetPos + GapPos [ID], MoveSpeed [ID] * DeltaTime);
	}
}

public struct J_RotateTransform : IJobParallelForTransform
{
	public Quaternion TargetRot;

	[ReadOnly] public NativeArray<Quaternion> GapRot;
	[ReadOnly] public NativeArray<float> RotateSpeed;
	public float DeltaTime;

	public void Execute (int ID, TransformAccess ThisTrans)
	{
		ThisTrans.rotation = Quaternion.Lerp (ThisTrans.rotation, TargetRot * GapRot [ID], RotateSpeed [ID] * DeltaTime);
	}
}
#endregion

#region Entity Region
class ObsMoveTarget : ComponentSystem
{
	struct Components
	{
		public ObsData ThisData;
		public RectTransform ThisTrans;
	}

	protected override void OnUpdate ( )
	{
		ComponentGroupArray<Components> getComps = GetEntities<Components> ( );
		if (getComps.Length == 0)
		{
			getComps.Dispose ( );
			return;
		}

		Components getCurrComp;
		ObsData getData;
		Transform getTrans;
		Vector3 getDirection;
		Vector3 getMove;

		float getTime = Time.deltaTime;
		int getLength = getComps.Length;

		for (int a = 0; a < getLength; a++)
		{
			getCurrComp = getComps [a];
			if (getCurrComp.ThisData.IsActive)
			{
				getData = getCurrComp.ThisData;
				getTrans = getCurrComp.ThisTrans;
				getDirection = getData.Direction;
				getMove = getDirection * getTime * getData.MoveSpeed;

				getTrans.localPosition += getMove;
				if (!getData.CheckDetect)
				{
					getData.CheckDetect = !checkDistance (getTrans.localPosition, getData.Destination, getData.SizeTarget);
				}
				else if (checkDistance (getTrans.localPosition, getData.Destination, getData.SizeTarget))
				{
					Manager.Game.gameOver ( );
				}
			}
		}

		getComps.Dispose ( );
	}

	bool checkDistance (Vector3 pos, Vector3 dest, float size)
	{
		if (Vector3.Distance (pos, dest) > size)
		{
			return true;
		}

		return false;
	}

	bool checkZero (Vector3 direction, Vector3 newPos)
	{
		if (direction.x > 0 && newPos.x > 0 || direction.x < 0 && newPos.x < 0)
		{
			return true;
		}
		else if (direction.y > 0 && newPos.y > 0 || direction.y < 0 && newPos.y < 0)
		{
			return true;
		}

		return false;
	}
}
#endregion