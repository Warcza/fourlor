﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

public class AudioManager : ManagerParent
{
	#region Variable
	public MIDIPlayer GetMidi;
	public MusicFX [ ] AllMF;
	public int TimeWaitMidi;
	Dictionary<AudioType, AllInfo> AllMusics;

	List<sourceInfo> ableSource;
	List<AllAudio> currCatAllAudio;

	string currCatName = "";

	class AllInfo
	{
		public Dictionary<string, Dictionary<string, AllAudio>> MusicTarget;
		public List<sourceInfo> MusicRunning;
		public GameObject audioParent;
		public float VolumeAudio;
	}

	struct sourceInfo
	{
		public CancellationTokenSource CancelToke;
		public AudioSource ThisAudio;
		public bool ForceDest;
	}
	#endregion

	#region Mono
	#endregion

	#region Public
	public AudioSource OpenAudio (AudioToken ThisTok)
	{
		AudioType thisType = ThisTok.ThisType;
		AllInfo thisInfo;

		if (!AllMusics.TryGetValue (thisType, out thisInfo))
		{
			return null;
		}

		string categoriName = ThisTok.CategorieName;
		Dictionary<string, AllAudio> getAudio;

		if (!thisInfo.MusicTarget.TryGetValue (categoriName, out getAudio))
		{
			return null;
		}

		AllAudio thisAudio;
		string thisName = ThisTok.ThisName;
		if (!getAudio.TryGetValue (thisName, out thisAudio))
		{
			return null;
		}

		System.Action thisAct = ThisTok.ThisAct;
		AudioAction audioAct = ThisTok.AudioAct;

		bool loopAudio = ThisTok.LoopAudio;
		int getLenght;

		if (audioAct != AudioAction.Nothing)
		{
			sourceInfo [ ] getAC = thisInfo.MusicRunning.ToArray ( );
			thisName = thisAudio.Audio.name;
			getLenght = getAC.Length;

			for (int a = 0; a < getLenght; a++)
			{
				if (getAC [a].ThisAudio.enabled && getAC [a].ThisAudio.clip.name == thisName)
				{
					if (audioAct == AudioAction.PlayIfEmpty)
					{
						return null;
					}
					else if (audioAct == AudioAction.Renew)
					{
						CloseThisAudio (thisType, categoriName, thisAudio.AudioName);
					}
				}
			}
		}

		AudioSource getAud;
#if UNITY_EDITOR
		getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );
#else

		if (ThisTok.OnThisPos != null)
		{
			getAud = ThisTok.OnThisPos.gameObject.AddComponent<AudioSource> ( );
		}
		else if (ableSource.Count > 0)
		{
			getAud = ableSource [ableSource.Count - 1].ThisAudio;
			ableSource.RemoveAt (ableSource.Count - 1);
			getAud.enabled = true;
		}
		else
		{
			getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );
		}
#endif

		getAud.volume = thisAudio.Volume * thisInfo.VolumeAudio;
		getAud.pitch = thisAudio.Pitch;
		getAud.clip = thisAudio.Audio;
		getAud.loop = loopAudio;
		getAud.enabled = true;
		sourceInfo getSource = new sourceInfo ( );
		getSource.ThisAudio = getAud;
		getSource.ForceDest = ThisTok.OnThisPos != null;
		getSource.CancelToke = new CancellationTokenSource ( );
		thisInfo.MusicRunning.Add (getSource);

		if (!loopAudio)
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			waitEndAudio (ThisTok.WaitStart, getAud.clip.length, thisInfo, getSource, thisAudio.NameMidi, thisAudio.BPM, ThisTok.CustomPath, thisAct, ThisTok.MidiActSpawn);
#else
			waitEndAudio (ThisTok.WaitStart, getAud.clip.length, thisInfo, getSource, thisAudio.TimeSpawn, thisAct, ThisTok.MidiActSpawn);
#endif
		}
		return getAud;
	}

	public void PlayerCustom (AudioToken ThisTok, AudioClip thisClip, byte [ ] data)
	{
#if UNITY_EDITOR || UNITY_STANDALONE
		AllInfo thisInfo;

		if (!AllMusics.TryGetValue (ThisTok.ThisType, out thisInfo))
		{
			return;
		}

		AllAudio thisAudio = new AllAudio ( );

		thisAudio.AudioName = ThisTok.ThisName;
		thisAudio.Volume = 1;
		thisAudio.Pitch = 1;
		thisAudio.Audio = thisClip;
		thisAudio.NameMidi = ThisTok.ThisName;
		thisAudio.BPM = 100;

		AudioSource getAud;
		getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );

		if (ThisTok.OnThisPos != null)
		{
			getAud = ThisTok.OnThisPos.gameObject.AddComponent<AudioSource> ( );
		}
		else if (ableSource.Count > 0)
		{
			getAud = ableSource [ableSource.Count - 1].ThisAudio;
			ableSource.RemoveAt (ableSource.Count - 1);
			getAud.enabled = true;
		}
		else
		{
			getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );
		}

		getAud.volume = thisAudio.Volume * thisInfo.VolumeAudio;
		getAud.pitch = thisAudio.Pitch;
		getAud.clip = thisAudio.Audio;
		getAud.loop = false;
		getAud.enabled = true;

		sourceInfo getSource = new sourceInfo ( );
		getSource.ThisAudio = getAud;
		getSource.ForceDest = ThisTok.OnThisPos != null;
		getSource.CancelToke = new CancellationTokenSource ( );
		thisInfo.MusicRunning.Add (getSource);

		waitEndAudio (ThisTok.WaitStart, getAud.clip.length, thisInfo, getSource, data, thisAudio.BPM, ThisTok.CustomPath, ThisTok.ThisAct, ThisTok.MidiActSpawn);

#endif
	}

	public AudioSource GetSource (AudioType thisType, string categorieName, string thisName)
	{
		AllInfo thisInfo;

		if (!AllMusics.TryGetValue (thisType, out thisInfo))
		{
			return null;
		}

		Dictionary<string, AllAudio> getAudio;
		if (!thisInfo.MusicTarget.TryGetValue (categorieName, out getAudio))
		{
			return null;
		}

		AllAudio thisAudio;
		if (!getAudio.TryGetValue (thisName, out thisAudio))
		{
			return null;
		}

		sourceInfo [ ] allSource = thisInfo.MusicRunning.ToArray ( );
		int getLenght = allSource.Length;

		for (int a = 0; a < getLenght; a++)
		{
			if (allSource [a].ThisAudio.enabled && allSource [a].ThisAudio.clip.name == thisName)
			{
				return allSource [a].ThisAudio;
			}
		}
		return null;
	}

	public void CloseThisAudio (AudioType thisType, string categorieName, string thisName)
	{
		AudioSource thisSource = GetSource (thisType, categorieName, thisName);
		if (thisSource != null)
		{
			AllInfo thisInfo;
			if (!AllMusics.TryGetValue (thisType, out thisInfo))
			{
				return;
			}

			int getLentgh = thisInfo.MusicRunning.Count;
			for (int a = 0; a < getLentgh; a++)
			{
				if (thisInfo.MusicRunning [a].ThisAudio == thisSource)
				{
					thisInfo.MusicRunning.RemoveAt (a);

					if (thisInfo.MusicRunning [a].CancelToke != null)
					{
						thisInfo.MusicRunning [a].CancelToke.Cancel ( );
						thisInfo.MusicRunning [a].CancelToke.Dispose ( );
					}

#if UNITY_EDITOR
					Destroy (thisSource);

#else
					if (thisInfo.MusicRunning [a].ForceDest)
					{
						Destroy (thisSource);
					}
					else
					{
						sourceInfo thisSourceinfo = new sourceInfo ( );
						thisSourceinfo.ForceDest = false;
						thisSourceinfo.ThisAudio = thisSource;
						ableSource.Add (thisSourceinfo);
						thisSourceinfo.ThisAudio.clip = null;
						thisSourceinfo.ThisAudio.enabled = false;
					}
#endif

					break;
				}
			}
		}
	}

	public void CloseAudio (AudioType thisType)
	{
		AllInfo thisInfo;

		if (!AllMusics.TryGetValue (thisType, out thisInfo))
		{
			return;
		}

		sourceInfo [ ] allSource = thisInfo.MusicRunning.ToArray ( );
		int getLenght = allSource.Length;

		for (int a = 0; a < getLenght; a++)
		{
			if (thisInfo.MusicRunning [a].CancelToke != null)
			{
				thisInfo.MusicRunning [a].CancelToke.Cancel ( );
			}

#if UNITY_EDITOR
			Destroy (allSource [a].ThisAudio);

#else	
			allSource [a].ThisAudio.clip = null;
			allSource [a].ThisAudio.enabled = false;
			ableSource.Add (allSource [a]);
#endif
		}

		thisInfo.MusicRunning.Clear ( );
	}

	public void CloseAllAudio ( )
	{
		AudioType [ ] getTypes = (AudioType [ ])System.Enum.GetValues (typeof (AudioType));
		for (int a = 0; a < getTypes.Length; a++)
		{
			CloseAudio (getTypes [a]);
		}
	}

	public string GetRandomMusic (AudioToken ThisTok, string [ ] musicsUsed)
	{
		AudioType thisType = ThisTok.ThisType;
		AllInfo thisInfo;

		if (!AllMusics.TryGetValue (thisType, out thisInfo))
		{
			return "";
		}

		string categoriName = ThisTok.CategorieName;
		Dictionary<string, AllAudio> getAudio;

		if (!thisInfo.MusicTarget.TryGetValue (categoriName, out getAudio) || getAudio.Count == 0)
		{
			return "";
		}

		if (currCatName != categoriName)
		{
			currCatName = categoriName;

			currCatAllAudio.Clear ( );
			List<AllAudio> setAudio = currCatAllAudio;
			foreach (var keyValue in getAudio)
			{
				setAudio.Add (keyValue.Value);
			}
		}

		int getLenght = musicsUsed.Length;
		int lengthAudio = currCatAllAudio.Count;

		if (getLenght > 0 && getLenght < lengthAudio)
		{
			List<AllAudio> newList = Manager.Garbage.ReInitList<AllAudio> (typeof (AllAudio), currCatAllAudio);

			int b;
			for (int a = 0; a < getLenght; a++)
			{
				for (b = lengthAudio; b > 0; b--)
				{
					if (musicsUsed [a] == newList [b].AudioName)
					{
						newList.RemoveAt (b);
						break;
					}
				}
			}

			return newList [Random.Range (0, newList.Count - 1)].AudioName;
		}

		return currCatAllAudio [Random.Range (0, currCatAllAudio.Count - 1)].AudioName;
	}

	public void stopMid ( )
	{
		GetMidi.StopSong ( );
	}
	#endregion

	#region Private
	protected override void InitializeManager ( )
	{
#if UNITY_EDITOR
		Object [ ] getAssets = Resources.LoadAll ("Scriptable/", typeof (AudioScriptable));
		Object [ ] getDetails = Resources.LoadAll ("Scriptable/Detail", typeof (DetailScriptable));

		AudioScriptable thisAudScript;
		List<MusicFX> getAllMusicF = new List<MusicFX> ( );

		List<AllDetail> standard = new List<AllDetail> ( );
		List<AllDetail> god = new List<AllDetail> ( );
		List<AllDetail> gray = new List<AllDetail> ( );
		AllDetail currDet = new AllDetail ( );
		GameType currType;

		for (int a = 0; a < getAssets.Length; a++)
		{
			thisAudScript = (getAssets [a] as AudioScriptable);
			getAllMusicF.Add (thisAudScript.ThisMusic);

			currDet.CategorieName = thisAudScript.ThisMusic.CategorieName;
			currDet.Lvl = thisAudScript.ThisMusic.Level;
			currType = thisAudScript.ThisMusic.ThisGameType;

			for (int b = 0; b < thisAudScript.ThisMusic.SetAudio.Length; b++)
			{
				currDet.AudioName = thisAudScript.ThisMusic.SetAudio [b].Audio.name;

				currDet.Score = 0;
				currDet.Perfect = false;

				if (currType == GameType.Standard)
				{
					standard.Add (currDet);
				}
				else if (currType == GameType.GodMode)
				{
					god.Add (currDet);
				}
				else
				{
					gray.Add (currDet);
				}
			}
		}

		for (int a = 0; a < getDetails.Length; a++)
		{
			DetailScriptable currDetail = (DetailScriptable)getDetails [a];

			currDetail.AllDetail = new AllDetail [0];
			switch (currDetail.ThisType)
			{
				case GameType.Standard:
					currDetail.AllDetail = standard.ToArray ( );
					break;

				case GameType.BlackWhith:
					currDetail.AllDetail = gray.ToArray ( );
					break;

				case GameType.GodMode:
					currDetail.AllDetail = gray.ToArray ( );
					break;
			}
		}

		AllMF = getAllMusicF.ToArray ( );
#endif

		AudioType [ ] getTypes = (AudioType [ ])System.Enum.GetValues (typeof (AudioType));
		currCatAllAudio = new List<AllAudio> ( );
		AudioType getAud;

		Dictionary<AudioType, AllInfo> setMusicTarget = new Dictionary<AudioType, AllInfo> (getTypes.Length);
		AllInfo thisInfo = new AllInfo ( );

		Transform currT = transform;

		ableSource = new List<sourceInfo> ( );

		Transform thisPar = transform;
		GameObject currObj;

		for (int a = 0; a < getTypes.Length; a++)
		{
			thisInfo = new AllInfo ( );
			thisInfo.MusicRunning = new List<sourceInfo> ( );

			getAud = getTypes [a];
			currObj = new GameObject ( );
			currObj.name = getAud.ToString ( );
			currObj.transform.SetParent (thisPar);

			thisInfo.audioParent = currObj;
			thisInfo.MusicTarget = addAllMusic (getAud);
			thisInfo.VolumeAudio = AllPlayerPrefs.GetFloatValue (getTypes [a].ToString ( ), 1);

			setMusicTarget.Add (getAud, thisInfo);
		}

		AllMusics = setMusicTarget;
	}

#if UNITY_EDITOR || UNITY_STANDALONE
	void launchMid (MIDIPlayer thisMidi, uint bpm, string nameMid, bool customPath = false, System.Action thisAction = null)
	{
		thisMidi.LoadSong (Constants._PathMid + "/" + nameMid + ".mid", bpm, thisAction);
	}

	void launchMid (MIDIPlayer thisMidi, uint bpm, byte [ ] nameMid, bool customPath = false, System.Action thisAction = null)
	{
		thisMidi.LoadSong (nameMid, bpm, thisAction);
	}
#endif

	Dictionary<string, Dictionary<string, AllAudio>> addAllMusic (AudioType thisType)
	{
		Dictionary<string, Dictionary<string, AllAudio>> SetAud = Manager.Garbage.ReInitDict<string, Dictionary<string, AllAudio>> (typeof (Dictionary<string, Dictionary<string, AllAudio>>));
		if (SetAud == null)
		{
			SetAud = new Dictionary<string, Dictionary<string, AllAudio>> ( );
		}

		Dictionary<string, AllAudio> thisAudio;

		MusicFX thisMusic;
		AllAudio value;

		int getLenghtB;
		int getLenghtC;
		int b;
		int c;

		getLenghtB = AllMF.Length;

		for (b = 0; b < getLenghtB; b++)
		{
			thisMusic = AllMF [b];

			if (thisMusic.ThisType == thisType)
			{
				if (SetAud.TryGetValue (thisMusic.CategorieName, out thisAudio))
				{
					getLenghtC = thisMusic.SetAudio.Length;
					for (c = 0; c < getLenghtC; c++)
					{
						if (!thisAudio.TryGetValue (thisMusic.SetAudio [c].AudioName, out value))
						{
							thisAudio.Add (thisMusic.SetAudio [c].AudioName, thisMusic.SetAudio [c]);
						}
						else
						{
							Debug.LogError ("Two audio with the same name on this Categorie : " + thisMusic.CategorieName);
						}
					}
				}
				else
				{
					thisAudio = Manager.Garbage.ReInitDict<string, AllAudio> (typeof (KeyValuePair<string, AllAudio>));
					if (thisAudio == null)
					{
						thisAudio = new Dictionary<string, AllAudio> ( );
					}
					getLenghtC = thisMusic.SetAudio.Length;
					for (c = 0; c < getLenghtC; c++)
					{
						thisAudio.Add (thisMusic.SetAudio [c].AudioName, thisMusic.SetAudio [c]);
					}

					SetAud.Add (thisMusic.CategorieName, new Dictionary<string, AllAudio> (thisAudio));
					thisAudio.Clear ( );
					Manager.Garbage.AddGarbageClass (thisAudio);
				}
			}
		}

		Dictionary<string, Dictionary<string, AllAudio>> saveAud = new Dictionary<string, Dictionary<string, AllAudio>> (SetAud);
		SetAud.Clear ( );
		Manager.Garbage.AddGarbageClass (SetAud);
		return saveAud;
	}

	void waitEndAudio (float TimeStart, float timeEnd, AllInfo thisInf, sourceInfo thisSource, int [ ] allSpawn, System.Action thisAct = null, System.Action midiAct = null)
	{
		GetMidi.AsyncLoadSong (allSpawn, midiAct);
		waitingAudio (TimeStart, timeEnd, thisInf, thisSource, thisAct);
	}

#if UNITY_EDITOR || UNITY_STANDALONE
	async void waitEndAudio (float TimeStart, float timeEnd, AllInfo thisInf, sourceInfo thisSource, string nameMidi = "", uint bpm = 0, bool customPath = false, System.Action thisAct = null, System.Action midiAct = null)
	{
		if (nameMidi != string.Empty && nameMidi != "")
		{
			/*
#if UNITY_EDITOR
			if (GetMidi.allRecord.Count > 0)
			{
				await Task.Delay (10000);
			}
#endif*/
			waitingAudio (TimeStart, timeEnd, thisInf, thisSource, thisAct);
			await Task.Delay (TimeWaitMidi);

			launchMid (GetMidi, bpm, nameMidi, customPath, midiAct);
		}
		else
		{
			waitingAudio (TimeStart, timeEnd, thisInf, thisSource, thisAct);
		}
	}

	async void waitEndAudio (float TimeStart, float timeEnd, AllInfo thisInf, sourceInfo thisSource, byte [ ] nameMidi, uint bpm = 0, bool customPath = false, System.Action thisAct = null, System.Action midiAct = null)
	{
		if (GetMidi.allRecord.Count > 0)
		{
			await Task.Delay (10000);
		}

		waitingAudio (TimeStart, timeEnd, thisInf, thisSource, thisAct);
		await Task.Delay (TimeWaitMidi);
		launchMid (GetMidi, bpm, nameMidi, customPath, midiAct);
	}
#endif

	async void waitingAudio (float TimeStart, float timeEnd, AllInfo thisInf, sourceInfo thisSource, System.Action thisAct)
	{
		CancellationToken newTok = thisSource.CancelToke.Token;

		try
		{
			await Task.Delay ((int)(TimeStart * 1000), newTok);
			thisSource.ThisAudio.Play ( );

			await Task.Delay ((int)(timeEnd * 1000), newTok);

			thisInf.MusicRunning.Remove (thisSource);

			if (thisAct != null)
			{
				stopMid ( );
				thisAct.Invoke ( );
			}

#if UNITY_EDITOR
			Destroy (thisSource.ThisAudio);
#else
			if (thisSource.ForceDest)
			{
				Destroy (thisSource.ThisAudio);
			}
			else
			{
				ableSource.Add (thisSource);
				thisSource.ThisAudio.clip = null;
				thisSource.ThisAudio.enabled = false;
			}
#endif
			thisSource.CancelToke.Dispose ( );
		}
		catch
		{
			thisSource.CancelToke.Dispose ( );
		}
	}
	#endregion
}