﻿#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

using UnityEngine;

public class AdsManager : ManagerParent
{
	#region Variables
#if UNITY_ANDROID 
	System.Action succeedAction;
	System.Action failAction;
#endif
	#endregion

	#region Mono
	#endregion

	#region Public Methods
	public void ShowRewardedAd (System.Action actSucceed = null, System.Action actFail = null)
	{
#if UNITY_ANDROID 
		if (Application.internetReachability != NetworkReachability.NotReachable && Advertisement.IsReady ("rewardedVideo"))
		{
			succeedAction = actSucceed;
			failAction = actFail;
			ShowOptions options = new ShowOptions
			{
				resultCallback = handleShowResult
			};

			Advertisement.Show ("rewardedVideo", options);
		}
		else if (actFail != null)
		{
			actFail.Invoke ( );
		}
#endif
	}

	public void ShowAdVideo (System.Action thisAct, System.Action actFail = null)
	{
#if UNITY_ANDROID 
		ShowOptions options = new ShowOptions
		{
			resultCallback = handleShowResult
		};

		if (Application.internetReachability != NetworkReachability.NotReachable && Advertisement.IsReady ( ))
		{
			succeedAction = thisAct;
			failAction = actFail;

			Advertisement.Show (options);
		}
		else if (actFail != null)
		{
			actFail.Invoke ( );
		}
#endif
	}
	#endregion

	#region Private Methods
	protected override void InitializeManager ( )
	{
		//Advertisement.Initialize ( "1307001", true );
	}
#if UNITY_ANDROID 
	void handleShowResult (ShowResult result)
	{
		switch (result)
		{
			case ShowResult.Finished:
				if (succeedAction != null)
				{
					succeedAction.Invoke ( );
				}
				break;

			case ShowResult.Skipped:
				if (failAction != null)
				{
					failAction.Invoke ( );
				}
				break;

			case ShowResult.Failed:
				if (failAction != null)
				{
					failAction.Invoke ( );
				}
				break;

			default:
				failAction.Invoke ( );
				break;
		}
	}
#endif
	#endregion
}