﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class UiManager : ManagerParent
{
	#region Variables
	public Color [ ] AllColor;
	public Color [ ] MenuColor;

	public Camera GetCam;
	public AnimationCurve DefaultAnime;

	[Header ("Info debug")]
	public Vector2 RectScreenSize;
	public Vector3 RatioScreen;

	Dictionary<MenuType, UiParent> AllMenu;
	List<UiParent> openMenu;
	#endregion

	#region Mono
	#endregion

	#region Public Methods
	public void DisplayMenu (MenuType MT, MenuTokenAbstract Source = null)
	{
		UiParent getCurrentOM;
		if (AllMenu.TryGetValue (MT, out getCurrentOM))
		{
			if (openMenu.Contains (getCurrentOM))
			{
				CloseMenu (MT);
			}

			if (openMenu.Count > 0)
			{
				openMenu [openMenu.Count - 1].IsActive = false;
			}

			openMenu.Add (getCurrentOM);

			getCurrentOM.OpenThis (Source);
		}
	}

	// Close Last open menu
	public void CloseMenu (int indexLast = 0)
	{
		if (openMenu.Count > indexLast)
		{
			UiParent getCurrentOM = openMenu [openMenu.Count - 1 - indexLast];

			getCurrentOM.CloseThis ( );
			openMenu.RemoveAt (openMenu.Count - 1 - indexLast);

			if (openMenu.Count > indexLast)
			{
				openMenu [openMenu.Count - 1 - indexLast].IsActive = true;
			}
		}
	}

	public void CloseAllMenu (MenuType activateBeforeType = MenuType.None, bool before = true)
	{
		UiParent getCurrentOM;

		if (before)
		{
			for (int a = 0; a < openMenu.Count - 1; a++)
			{
				getCurrentOM = openMenu [a];

				if (getCurrentOM.ThisType != activateBeforeType)
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					a--;
				}
				else
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					a--;
					return;
				}
			}
		}
		else
		{
			for (int a = openMenu.Count - 1; a >= 0; a--)
			{
				getCurrentOM = openMenu [a];

				if (getCurrentOM.ThisType != activateBeforeType)
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
				}
				else
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					return;
				}
			}
		}
	}

	// Close spec menu
	public void CloseMenu (MenuType MT)
	{
		UiParent getCurrentOM;

		if (AllMenu.TryGetValue (MT, out getCurrentOM))
		{
			getCurrentOM.CloseThis ( );

			if (openMenu.Contains (getCurrentOM))
			{
				if (openMenu.IndexOf (getCurrentOM) == openMenu.Count - 1 && openMenu.Count > 1)
				{
					openMenu [openMenu.Count - 2].IsActive = true;
				}

				openMenu.Remove (getCurrentOM);
			}
		}
	}

	// Return the top of one canvas order + 1
	public int LastCanvasOrder ( )
	{
		int getMaxOrder = 0;

		for (int a = 0; a < openMenu.Count; a++)
		{
			try
			{
				if (openMenu [a].GetComponent<Canvas> ( ).sortingOrder >= getMaxOrder)
				{
					getMaxOrder = openMenu [a].GetComponent<Canvas> ( ).sortingOrder + 1;
				}
			}
			catch
			{ }
		}

		return getMaxOrder;
	}

	#region SimpleOperation
	public void UpdateScaleChild (Transform thisParent)
	{
		int getCountChild = thisParent.childCount;

		for (int a = 0; a < getCountChild; a++)
		{
			thisParent.GetChild (a).localScale = RatioScreen;
		}
	}
	#endregion
	#region TransformInterface
	public void randPos (IList<Transform> ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		setRandPos (ArrayTrans, sizeRect, forceBoth, randomSideX, randomSideY);
	}

	public void randPos (Transform getTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		setRandPos (getTrans, sizeRect, forceBoth, randomSideX, randomSideY);
	}

	public void UisPosition (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiPos (ArrayTrans, getPos, timePlacement, thisAct, waitPourc, thisAnime, thisTok);
	}

	public void UiPosition (Transform thisTrans, Vector3 thisPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiPos (thisTrans, thisPos, timePlacement, thisAct, waitPourc, thisAnime, thisTok);
	}

	public void UisScale (IList<Transform> ArrayTrans, Vector3 [ ] arraySize, float timePlacement, System.Action thisAct = null, bool backNormal = false, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeScaleTrans (ArrayTrans, arraySize, timePlacement, thisAct, backNormal, saveArraySize, waitPourc, thisAnime, thisTok);
	}

	public void UiScale (Transform thisTrans, Vector3 thisSize, Vector3 saveSize, float timePlacement, System.Action thisAct = null, bool backNormal = false, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeScaleTrans (thisTrans, thisSize, timePlacement, thisAct, backNormal, saveSize, waitPourc, thisAnime, thisTok);
	}
	#endregion

	#region ColorInterface
	public void ChangeColorInstant (IList<Transform> ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		SetInstantColor (ArrayTrans, RemoveThis, OtherColor);
	}

	public void ChangeColorInstant (Transform getTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		SetInstantColor (getTrans, RemoveThis, OtherColor);
	}

	public void ChangeColor (IList<Transform> ArrayTrans, float timePlacement, System.Action thisAct = null, float waitPourc = 100, Color [ ] RemoveThis = null, Color [ ] OtherColor = null, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiColor (ArrayTrans, timePlacement, thisAct, waitPourc, RemoveThis, OtherColor, thisAnime, thisTok);
	}

	public void ChangeColor (Transform thisTrans, float timePlacement, System.Action thisAct = null, float waitPourc = 100, Color [ ] RemoveThis = null, Color [ ] OtherColor = null, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiColor (thisTrans, timePlacement, thisAct, waitPourc, RemoveThis, OtherColor, thisAnime, thisTok);
	}

	public void GoThisColor (IList<Transform> ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100)
	{
		goThisColor (ArrayTrans, thisColor, timePlacement, thisAct, waitPourc);
	}

	public void GoThisColor (Transform thisTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100)
	{
		goThisColor (thisTrans, thisColor, timePlacement, thisAct, waitPourc);
	}
	#endregion

	#endregion

	#region Private Methods
	protected override void InitializeManager ( )
	{
		RectScreenSize = new Vector2 (Screen.width, Screen.height) * 1.01f;
		RatioScreen = Vector3.one * (RectScreenSize.x / 1920);

		Object [ ] getAllMenu = Resources.LoadAll ("MenuUI");
		Dictionary<MenuType, UiParent> setAllMenu = new Dictionary<MenuType, UiParent> (getAllMenu.Length);
		openMenu = new List<UiParent> (getAllMenu.Length);

		GameObject thisMenu;
		UiParent thisUi;
		Transform getParent = transform;

		for (int a = 0; a < getAllMenu.Length; a++)
		{
			thisMenu = (GameObject)Instantiate (getAllMenu [a], getParent);
			thisUi = thisMenu.GetComponent<UiParent> ( );
			thisUi.GetComponent<Canvas> ( ).worldCamera = GetCam;
			thisUi.Initialize ( );
			thisMenu.SetActive (false);
			setAllMenu.Add (thisUi.ThisType, thisUi);
		}

		AllMenu = setAllMenu;

		DisplayMenu (MenuType.Home);
	}

	#region ColorUpdate
	void SetInstantColor (IList<Transform> ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		int getLength;
		int getRand;
		int a;

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getLength = ArrayTrans.Count;

		for (a = 0; a < getLength; a++)
		{
			getRand = Random.Range (0, getCol.Count);
			ArrayTrans [a].GetComponent<Image> ( ).color = getCol [getRand];
			getCol.RemoveAt (getRand);
		}

		Manager.Garbage.AddGarbageClass (ArrayTrans);
		Manager.Garbage.AddGarbageClass (getCol);
	}

	void SetInstantColor (Transform ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		int getLength;
		int getRand;

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (int a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getRand = Random.Range (0, getCol.Count);
		ArrayTrans.GetComponent<Image> ( ).color = getCol [getRand];
		getCol.RemoveAt (getRand);

		Manager.Garbage.AddGarbageClass (getCol);
	}

	async void changeUiColor (IList<Transform> ArrayTrans, float timePlacement, System.Action thisAct, float waitPourc, Color [ ] RemoveThis, Color [ ] OtherColor, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float getTime = 15 * 0.001f;
		float time = 0;

		int getLength;
		int getRand;
		int a;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getLength = ArrayTrans.Count;
		List<Color> currCol = Manager.Garbage.ReInitList<Color> (typeof (Color), getLength);
		List<Image> currImage = Manager.Garbage.ReInitList<Image> (typeof (Image), getLength);

		for (a = 0; a < getLength; a++)
		{
			getRand = Random.Range (0, getCol.Count);

			currImage.Add (ArrayTrans [a].GetComponent<Image> ( ));
			currCol.Add (getCol [getRand]);
			getCol.RemoveAt (getRand);
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					if (time < 1 && !checkColor (currImage [a].color, currCol [a]))
					{
						currImage [a].color = Color.LerpUnclamped (currImage [a].color, currCol [a], thisCurve.Evaluate (time));
					}
					else
					{
						currImage [a].color = currCol [a];
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
					Manager.Garbage.AddGarbageClass (currImage);
					Manager.Garbage.AddGarbageClass (currCol);
					Manager.Garbage.AddGarbageClass (getCol);
					Manager.Garbage.AddGarbageClass (ArrayTrans);
				}
			}
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (currCol);
			Manager.Garbage.AddGarbageClass (getCol);
		}
	}

	async void changeUiColor (Transform ArrayTrans, float timePlacement, System.Action thisAct, float waitPourc, Color [ ] RemoveThis, Color [ ] OtherColor, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float getTime = 15 * 0.001f;
		float time = 0;

		int getLength;
		int getRand;
		int a;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getRand = Random.Range (0, getCol.Count);
		Color currCol = getCol [getRand];
		Image currImage = ArrayTrans.GetComponent<Image> ( );

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				if (time < 1 && !checkColor (currImage.color, currCol))
				{
					currImage.color = Color.LerpUnclamped (currImage.color, currCol, thisCurve.Evaluate (time));
				}
				else
				{
					currImage.color = currCol;
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
					Manager.Garbage.AddGarbageClass (currImage);
					Manager.Garbage.AddGarbageClass (ArrayTrans);
				}
			}
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (getCol);
		}
	}

	async void goThisColor (IList<Transform> ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float getTime = 15 * 0.001f;
		float time = 0;

		int getLength = ArrayTrans.Count;
		List<Image> currImage = Manager.Garbage.ReInitList<Image> (typeof (Image), getLength);

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		for (int a = 0; a < getLength; a++)
		{
			currImage.Add (ArrayTrans [a].GetComponent<Image> ( ));
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				for (int a = 0; a < getLength; a++)
				{
					if (time < 1)
					{
						currImage [a].color = Color.LerpUnclamped (currImage [a].color, thisColor, thisCurve.Evaluate (time));
					}
					else
					{
						currImage [a].color = thisColor;
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}

			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	async void goThisColor (Transform ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float getTime = 15 * 0.001f;
		float time = 0;

		Image currImage = ArrayTrans.GetComponent<Image> ( );

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				if (time < 1)
				{
					currImage.color = Color.LerpUnclamped (currImage.color, thisColor, thisCurve.Evaluate (time));
				}
				else
				{
					currImage.color = thisColor;
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}

			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	bool checkColor (Color color1, Color color2)
	{
		if (Mathf.Abs (color1.r - color2.r) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.b - color2.b) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.g - color2.g) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.a - color2.a) > 0.001f)
		{
			return false;
		}

		return true;
	}
	#endregion

	#region TransformUpdate
	void setRandPos (IList<Transform> ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		Vector3 getCurrPos;
		Vector2 getNewPos;

		int getRand;
		int getLength = ArrayTrans.Count;

		for (int a = 0; a < getLength; a++)
		{
			ArrayTrans [a].GetComponent<Image> ( ).color = Color.black;
			getCurrPos = ArrayTrans [a].localPosition;
			getNewPos = newPos (getCurrPos, sizeRect);

			if (!forceBoth)
			{
				getRand = Random.Range (0, 2);

				if (getRand == 0)
				{
					getRand = Random.Range (0, 2);

					if (getRand == 0)
					{
						getNewPos = new Vector2 (0, getNewPos.y);
					}
					else
					{
						getNewPos = new Vector2 (getNewPos.x, 0);
					}
				}
			}

			if (randomSideX)
			{
				if (Random.Range (0, 2) == 0)
				{
					getNewPos = new Vector3 (-getNewPos.x, getNewPos.y);
				}
			}

			if (randomSideY)
			{
				if (Random.Range (0, 2) == 0)
				{
					getNewPos = new Vector3 (getNewPos.x, -getNewPos.y);
				}
			}

			getCurrPos = new Vector3 (getCurrPos.x + getNewPos.x, getCurrPos.y + getNewPos.y, getCurrPos.z);

			ArrayTrans [a].localPosition = getCurrPos;
		}
		Manager.Garbage.AddGarbageClass (ArrayTrans);
	}

	void setRandPos (Transform ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		Vector3 getCurrPos;
		Vector2 getNewPos;

		int getRand;

		ArrayTrans.GetComponent<Image> ( ).color = Color.black;
		getCurrPos = ArrayTrans.localPosition;
		getNewPos = newPos (getCurrPos, sizeRect);

		if (!forceBoth)
		{
			getRand = Random.Range (0, 2);

			if (getRand == 0)
			{
				getRand = Random.Range (0, 2);

				if (getRand == 0)
				{
					getNewPos = new Vector2 (0, getNewPos.y);
				}
				else
				{
					getNewPos = new Vector2 (getNewPos.x, 0);
				}
			}
		}

		if (randomSideX)
		{
			if (Random.Range (0, 2) == 0)
			{
				getNewPos = new Vector3 (-getNewPos.x, getNewPos.y);
			}
		}

		if (randomSideY)
		{
			if (Random.Range (0, 2) == 0)
			{
				getNewPos = new Vector3 (getNewPos.x, -getNewPos.y);
			}
		}

		getCurrPos = new Vector3 (getCurrPos.x + getNewPos.x, getCurrPos.y + getNewPos.y, getCurrPos.z);

		ArrayTrans.localPosition = getCurrPos;
	}

	async void changeUiPos (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		await Task.Delay (15);

		List<Vector3> startPos = Manager.Garbage.ReInitList<Vector3> (typeof (Vector3));
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float getTime = 15 * 0.001f;
		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		for (a = 0; a < getLength; a++)
		{
			startPos.Add (ArrayTrans [a].localPosition);
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					currPos = ArrayTrans [a].localPosition;

					if (time < 1 && (ArrayTrans [a].localPosition - getPos [a]).magnitude > 0.05f)
					{
						ArrayTrans [a].localPosition = Vector3.LerpUnclamped (startPos [a], getPos [a], thisCurve.Evaluate (time));
					}
					else
					{
						ArrayTrans [a].localPosition = new Vector3 (getPos [a].x, getPos [a].y, currPos.z);
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (startPos);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (startPos);
		}
	}

	async void changeUiPos (Transform ArrayTrans, Vector3 getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		await Task.Delay (15);

		Vector3 startPos = ArrayTrans.localPosition;
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float getTime = 15 * 0.001f;
		float time = 0;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				currPos = ArrayTrans.localPosition;

				if (time < 1 && (ArrayTrans.localPosition - getPos).magnitude > 0.05f)
				{
					ArrayTrans.localPosition = Vector3.LerpUnclamped (startPos, getPos, thisCurve.Evaluate (time));
				}
				else
				{
					ArrayTrans.localPosition = new Vector3 (getPos.x, getPos.y, currPos.z);
				}

				if (thisAct != null && (time * 100 >= waitPourc))
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	async void changeScaleTrans (IList<Transform> ArrayTrans, Vector3 [ ] getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float getTime = 15 * 0.001f;
		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					currPos = ArrayTrans [a].localScale;

					if (time < 1 && (currPos - getSize [a]).magnitude > 0.05f)
					{
						ArrayTrans [a].localScale = Vector3.LerpUnclamped (saveArraySize [a], getSize [a], thisCurve.Evaluate (time));
					}
					else
					{
						ArrayTrans [a].localScale = new Vector3 (getSize [a].x, getSize [a].y, currPos.z);
					}
				}

				if (time * 100 >= waitPourc)
				{
					if (backNormal)
					{
						backNormal = false;
						time = 0;

						Vector3 [ ] defaultSize = saveArraySize;
						saveArraySize [a] = getSize [a];
						getSize = defaultSize;
					}
					else if (thisAct != null)
					{
						thisAct.Invoke ( );
						thisAct = null;
					}
				}
			}
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	async void changeScaleTrans (Transform thisTrans, Vector3 getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 saveArraySize, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float getTime = 15 * 0.001f;
		float time = 0;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (15, thisTok.Token);
				}
				else
				{
					await Task.Delay (15);
				}

				time += getTime / timePlacement;

				currPos = thisTrans.localScale;

				if (time < 1 && (currPos - getSize).magnitude > 0.05f)
				{
					thisTrans.localScale = Vector3.LerpUnclamped (saveArraySize, getSize, thisCurve.Evaluate (time));
				}
				else
				{
					thisTrans.localScale = new Vector3 (getSize.x, getSize.y, currPos.z);
				}

				if (time * 100 >= waitPourc)
				{
					if (backNormal)
					{
						backNormal = false;
						time = 0;
						Vector3 defaultSize = saveArraySize;
						saveArraySize = getSize;
						getSize = defaultSize;
					}
					else if (thisAct != null)
					{
						thisAct.Invoke ( );
						thisAct = null;
					}
				}
			}
		}
		catch
		{ }
	}

	Vector2 newPos (Vector3 thisPos, Vector2 getRect)
	{
		Vector2 setRect = getRect;
		if (thisPos.y < 0)
		{
			setRect = new Vector2 (setRect.x, -getRect.y);
		}
		else
		{
			setRect = new Vector2 (setRect.x, getRect.y);
		}

		if (thisPos.x < 0)
		{
			setRect = new Vector2 (-getRect.x, setRect.y);
		}
		else
		{
			setRect = new Vector2 (getRect.x, setRect.y);
		}

		return setRect;
	}
	#endregion

	#endregion
}