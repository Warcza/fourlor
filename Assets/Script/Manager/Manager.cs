﻿using UnityEngine;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{
	#region Variables
	static Manager mainManagerInstance;
	//Add new managers here
	static EventManager evnt;
	public static EventManager Event
	{
		get
		{
			return evnt;
		}
	}

	static GameManger game;
	public static GameManger Game
	{
		get
		{
			return game;
		}
	}

	static UiManager ui;
	public static UiManager UI
	{
		get
		{
			return ui;
		}
	}

	static AudioManager aud;
	public static AudioManager Aud
	{
		get
		{
			return aud;
		}
	}

	static JobsManager job;
	public static JobsManager Job
	{
		get
		{
			return job;
		}
	}

	static AdsManager ads;
	public static AdsManager ADS
	{
		get
		{
			return ads;
		}
	}

	static GarbageManager garbage;
	public static GarbageManager Garbage
	{
		get
		{
			return garbage;
		}
	}
	static public string saveMidiFile;
	#endregion

	#region Mono
	void Awake ( )
	{
		PlayerPrefs.DeleteAll ( );

		//Cursor.lockState = CursorLockMode.Locked;
		Application.targetFrameRate = 60;

		//Keep manager a singleton
		if (mainManagerInstance != null)
		{
			Destroy (gameObject);
		}
		else
		{
			DontDestroyOnLoad (gameObject);
			mainManagerInstance = this;
			InitializeManagers ( );
		}
	}
	#endregion

	#region Public Methods

	#endregion

	#region Private Methods
	void InitializeManagers ( )
	{
		InitializeManager (ref garbage);
		InitializeManager (ref evnt);
		InitializeManager (ref ui);
		InitializeManager (ref aud);
		InitializeManager (ref game);
		InitializeManager (ref job);
		InitializeManager (ref ads);
	}

	void InitializeManager<T> (ref T manager)where T : ManagerParent
	{
		Debug.Log ("Initializing managers");
		T [ ] managers = GetComponentsInChildren<T> ( );

		if (managers.Length == 0)
		{
			Debug.LogError ("No manager of type: " + typeof (T) + " found.");
			return;
		}

		//Set to first manager
		manager = managers [0];
		manager.Initialize ( );

		if (managers.Length > 1) //Too many managers
		{
			Debug.LogError ("Found " + managers.Length + " managers of type " + typeof (T));
			for (int i = 1; i < managers.Length; i++)
			{
				Destroy (managers [i].gameObject);
			}
		}
	}
	#endregion
}