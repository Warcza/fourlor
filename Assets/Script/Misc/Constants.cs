﻿using System.Collections.Generic;

using UnityEngine;

public static class Constants
{
	#region Scene
	#endregion

	#region Tag
	#endregion

	#region Layer
	#endregion

	#region others
	public const string _AdCount = "AdCount";
	public const string _MainParentRoom = "MainParent";
	public const string _Day = "DAY";
	public const string _Month = "MONTH";
	public const string _Year = "YEAR";

	public const float F_RatioLess = 0.9965f;
	#endregion

	#region playerPrefs
	#endregion

	#region path
	public const string _PathMid = "Audio/Midis";
	#endregion
}