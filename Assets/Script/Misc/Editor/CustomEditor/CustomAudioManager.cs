﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (AudioManager))]
public class CustomAudioManager : Editor
{
	#region Variables
	SerializedProperty AllMF;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void OnEnable ( )
	{
		AllMF = serializedObject.FindProperty ("AllMF");
	}

	public override void OnInspectorGUI ( )
	{
		AudioManager myTarget = (AudioManager)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		if (GUILayout.Button ("Update Scriptable Audio"))
		{
			Object [ ] getAssets = Resources.LoadAll ("Scriptable/", typeof (AudioScriptable));
			Object [ ] getDetails = Resources.LoadAll ("Scriptable/Detail", typeof (DetailScriptable));

			AudioScriptable thisAudScript;
			List<MusicFX> getAllMusicF = new List<MusicFX> ( );

			List<AllDetail> standard = new List<AllDetail> ( );
			List<AllDetail> god = new List<AllDetail> ( );
			List<AllDetail> gray = new List<AllDetail> ( );
			AllDetail currDet = new AllDetail ( );
			GameType currType;

			for (int a = 0; a < getAssets.Length; a++)
			{
				thisAudScript = (getAssets [a] as AudioScriptable);
				getAllMusicF.Add (thisAudScript.ThisMusic);

				currDet.CategorieName = thisAudScript.ThisMusic.CategorieName;
				currDet.Lvl = thisAudScript.ThisMusic.Level;
				currType = thisAudScript.ThisMusic.ThisGameType;

				for (int b = 0; b < thisAudScript.ThisMusic.SetAudio.Length; b++)
				{
					currDet.AudioName = thisAudScript.ThisMusic.SetAudio [b].Audio.name;

					currDet.Score = 0;
					currDet.Perfect = false;

					if (currType == GameType.Standard)
					{
						standard.Add (currDet);
					}
					else if (currType == GameType.GodMode)
					{
						god.Add (currDet);
					}
					else
					{
						gray.Add (currDet);
					}
				}
			}

			for (int a = 0; a < getDetails.Length; a++)
			{
				DetailScriptable currDetail = (DetailScriptable)getDetails [a];

				currDetail.AllDetail = new AllDetail [0];
				switch (currDetail.ThisType)
				{
					case GameType.Standard:
						currDetail.AllDetail = standard.ToArray ( );
						break;

					case GameType.BlackWhith:
						currDetail.AllDetail = gray.ToArray ( );
						break;

					case GameType.GodMode:
						currDetail.AllDetail = gray.ToArray ( );
						break;
				}
			}

			myTarget.AllMF = getAllMusicF.ToArray ( );
		}

		if (GUILayout.Button ("Random Audio") && Application.isPlaying)
		{
			AudioToken thisTok = new AudioToken ( );
			thisTok.AudioAct = AudioAction.Renew;
			thisTok.CategorieName = "Menu";
			thisTok.LoopAudio = false;
			thisTok.ThisAct = test;
			thisTok.ThisName = "";
			thisTok.ThisType = AudioType.MBG;

			myTarget.OpenAudio (thisTok);
		}

		EditorGUILayout.Space ( );

		EditorGUILayout.PropertyField (AllMF, true);

		EditorGUILayout.Space ( );
		DrawDefaultInspector ( );

		serializedObject.ApplyModifiedProperties ( );
	}
	#endregion

	#region Private Methodes
	void test ( )
	{
		AudioToken thisTok = new AudioToken ( );
		thisTok.AudioAct = AudioAction.Nothing;
		thisTok.CategorieName = "Menu";
		thisTok.LoopAudio = false;
		thisTok.ThisAct = null;
		thisTok.ThisName = "";
		thisTok.ThisType = AudioType.MBG;

		AudioManager myTarget = (AudioManager)target;

		myTarget.OpenAudio (thisTok);
	}
	#endregion
}