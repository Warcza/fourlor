﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (MIDIPlayer))]
public class CustomMidiPlaye : Editor
{
	#region Variables
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
#if UNITY_EDITOR
	public override void OnInspectorGUI ( )
	{
		MIDIPlayer myTarget = (MIDIPlayer)target;

		serializedObject.Update ( );
		DrawDefaultInspector ( );
		serializedObject.ApplyModifiedProperties ( );
	}
#endif
	#endregion

	#region Private Methodes

	#endregion
}