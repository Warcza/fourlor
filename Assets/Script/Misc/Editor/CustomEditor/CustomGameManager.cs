﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (GameManger))]
public class CustomGameManager : Editor
{
	#region Variables
	SerializedProperty AllLevel;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void OnEnable ( )
	{
		AllLevel = serializedObject.FindProperty ("AllLevel");
	}

	public override void OnInspectorGUI ( )
	{
		GameManger myTarget = (GameManger)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		if (GUILayout.Button ("Update Scriptable Level"))
		{
			Object [ ] getAssets = Resources.LoadAll ("Scriptable/LevelEdit", typeof (LevelScriptable));
			Object [ ] getAssetsAudio = Resources.LoadAll ("Scriptable/", typeof (AudioScriptable));

			LevelScriptable thisLevelScript;
			List<LevelInfo> getAllLvl = new List<LevelInfo> ( );
			List<string> currCat;
			int a;

			for (a = 0; a < getAssets.Length; a++)
			{
				thisLevelScript = (getAssets [a] as LevelScriptable);
				currCat = new List<string> ( );

				for (int b = 0; b < getAssetsAudio.Length; b++)
				{
					AudioScriptable thisAud = (AudioScriptable)getAssetsAudio [b];

					for (int c = 0; c < thisLevelScript.ThisLevel.LvlList.Length; c++)
					{
						if (thisAud.ThisMusic.Level == thisLevelScript.ThisLevel.LvlList [c])
						{
							currCat.Add (thisAud.ThisMusic.CategorieName);
							break;
						}
					}
				}

				thisLevelScript.ThisLevel.CategorieName = currCat.ToArray ( );
				getAllLvl.Add (thisLevelScript.ThisLevel);
			}

			myTarget.AllLevel = getAllLvl.ToArray ( );
		}

		serializedObject.ApplyModifiedProperties ( );
		DrawDefaultInspector ( );
	}
	#endregion

	#region Private Methodes
	#endregion
}