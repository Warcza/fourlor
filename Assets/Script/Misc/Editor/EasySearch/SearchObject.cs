﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEditor;

using UnityEngine;

public class SearchObject : MonoBehaviour
{
	#region Variables
	#endregion

	#region Mono
	#endregion

	#region Public Methods
	public static IEnumerator LoadAssetsInProject (List<ObjectParent> objectList, ResearcheType thisType, Object objComp, bool getChildren, InfoResearch thisResearch)
	{
		TypePlace currPlace = TypePlace.OnProject;

		if (thisType == ResearcheType.Object_Prefab || thisType == ResearcheType.Reference)
		{
			if (objComp == null)
			{
				EasySearch.EndResearch (currPlace);
				yield break;
			}
		}

		List<GameObject> asset = new List<GameObject> ( );
		WaitForEndOfFrame thisF = new WaitForEndOfFrame ( );

		string guid;
		string assetPath;

		string [ ] GUIDs;
		string optionalPath = thisResearch.FolderProject;

		int a;

		if (optionalPath != "")
		{
			if (optionalPath.EndsWith ("/"))
			{
				optionalPath = optionalPath.TrimEnd ('/');
			}
			GUIDs = AssetDatabase.FindAssets ("t:GameObject", new string [ ]
			{
				optionalPath
			});
		}
		else
		{
			GUIDs = AssetDatabase.FindAssets ("t:GameObject");
		}

		EasySearch.AssetLoading (true, GUIDs.Length);

		for (a = 0; a < GUIDs.Length; a++)
		{
			EasySearch.PlusAssetLoaded ( );
			guid = GUIDs [a];
			assetPath = AssetDatabase.GUIDToAssetPath (guid);
			asset.Add (AssetDatabase.LoadAssetAtPath (assetPath, typeof (GameObject))as GameObject);
			yield return thisF;
		}

		EasySearch.AssetLoading (false, GUIDs.Length);

		if (asset.Count == 0)
		{
			EasySearch.EndResearch (currPlace);
			yield break;
		}

		if (getChildren)
		{
			int countMax = 0;
			for (a = 0; a < asset.Count; a++)
			{
				countMax += GetComponentsInChildrenOfAsset (asset [a]).Length;
			}

			EasySearch.MaxCount (countMax, currPlace);

			for (a = 0; a < asset.Count; a++)
			{
				returnCurrObj (currPlace, objectList, GetComponentsInChildrenOfAsset (asset [a]), thisType, objComp, thisResearch);
				yield return thisF;
			}
		}
		else
		{
			EasySearch.MaxCount (asset.Count, currPlace);

			returnCurrObj (currPlace, objectList, asset.ToArray ( ), thisType, objComp, thisResearch);
		}

		yield return thisF;
	}

	public static IEnumerator LoadAssetOnScenes (List<ObjectParent> getAllObj, ResearcheType thisType, Object objComp, bool getChildren, InfoResearch thisResearch)
	{
		TypePlace currPlace = TypePlace.OnScene;

		if (thisType == ResearcheType.Object_Prefab || thisType == ResearcheType.Reference)
		{
			if (objComp == null)
			{
				EasySearch.EndResearch (currPlace);
				yield break;
			}
		}

		GameObject [ ] objectList = UnityEngine.SceneManagement.SceneManager.GetActiveScene ( ).GetRootGameObjects ( );
		WaitForEndOfFrame thisF = new WaitForEndOfFrame ( );

		if (objectList.Length == 0)
		{
			EasySearch.EndResearch (currPlace);
			yield break;
		}

		if (getChildren)
		{
			int a;
			int countMax = 0;
			for (a = 0; a < objectList.Length; a++)
			{
				countMax += GetComponentsInChildrenOfAsset (objectList [a]).Length;
			}

			EasySearch.MaxCount (countMax, currPlace);

			for (a = 0; a < objectList.Length; a++)
			{
				returnCurrObj (currPlace, getAllObj, GetComponentsInChildrenOfAsset (objectList [a]), thisType, objComp, thisResearch);
				yield return thisF;
			}
		}
		else
		{
			EasySearch.MaxCount (objectList.Length, currPlace);

			returnCurrObj (currPlace, getAllObj, objectList, thisType, objComp, thisResearch);
		}

		yield return thisF;
	}

	public static IEnumerator LoadOnPrefab (List<ObjectParent> objectList, ResearcheType thisType, Object objComp, List<GameObject> thisPref, bool getChildren, InfoResearch thisResearch, TypePlace currPlace = TypePlace.OnObject)
	{
		if (thisType == ResearcheType.Object_Prefab || thisType == ResearcheType.Reference)
		{
			if (objComp == null)
			{
				EasySearch.EndResearch (currPlace);
				yield break;
			}
		}

		WaitForEndOfFrame thisF = new WaitForEndOfFrame ( );

		int a;

		if (thisPref.Count == 0)
		{
			EasySearch.EndResearch (currPlace);
			yield break;
		}

		if (getChildren)
		{
			int countMax = 0;
			for (a = 0; a < thisPref.Count; a++)
			{
				if (thisPref [a] != null)
				{
					countMax += GetComponentsInChildrenOfAsset (thisPref [a]).Length;
				}
				else
				{
					countMax++;
				}
			}

			EasySearch.MaxCount (countMax, currPlace);

			for (a = 0; a < thisPref.Count; a++)
			{
				if (thisPref [a] != null)
				{
					returnCurrObj (currPlace, objectList, GetComponentsInChildrenOfAsset (thisPref [a]), thisType, objComp, thisResearch);
					yield return thisF;
				}
			}
		}
		else
		{
			EasySearch.MaxCount (thisPref.Count, currPlace);

			returnCurrObj (currPlace, objectList, thisPref.ToArray ( ), thisType, objComp, thisResearch);
		}

		yield return thisF;
	}
	#endregion

	#region Private Methods
	static async void returnCurrObj (TypePlace thisPlace, List<ObjectParent> CurrList, GameObject [ ] objectList, ResearcheType thisType, Object objComp, InfoResearch thisResearch)
	{
		await Task.Delay (0);
		//WaitForEndOfFrame thisF = new WaitForEndOfFrame ( );

		List<GameObject> objTagList = new List<GameObject> ( );
		Component [ ] components;
		Component [ ] componentsPref;

		//System.Reflection.PropertyInfo [] getAdvanceProp = thisResearch.properCheck.ToArray ( );
		System.Reflection.FieldInfo [ ] getAdvanceField = thisResearch.fieldCheck.ToArray ( );

		System.Reflection.FieldInfo [ ] getFields;

		GameObject getPref;
		object currField;

		string thisStringSearch = thisResearch.StringSearch;
		string OtherName = thisResearch.OtherName;
		string getCompName;

		int diffComp = thisResearch.NbrCompDiff;
		int diffChil = thisResearch.NbrChildDiff;

		bool getProper = false;
		bool checkRef;
		bool samePref = false;
		bool approxName = thisResearch.LargeName;
		bool checkComp = thisResearch.checkFieldComp;

		if (thisType == ResearcheType.Object_Prefab || thisType == ResearcheType.Reference)
		{
			getProper = thisResearch.TryGetProperty;
			samePref = thisResearch.SameObj;

			try
			{
				getPref = (GameObject)objComp;
				componentsPref = getPref.GetComponents<Component> ( );
			}
			catch
			{
				getPref = null;
				componentsPref = null;
			}
		}
		else
		{
			getPref = objectList [0];
			componentsPref = getPref.GetComponents<Component> ( );
		}

		string getSearch = thisStringSearch;

		int a;
		int b;
		int c;
		int d;
		int countComp;
		for (a = 0; a < objectList.Length; a++)
		{
			EasySearch.AddCount (thisPlace);
			//yield return thisF;

			if (objectList [a] == null)
			{
				continue;
			}
			switch (thisType)
			{
				case ResearcheType.Tag:
					if (getSearch == string.Empty || objectList [a].tag == getSearch)
					{
						objTagList.Add (objectList [a]);
					}
					break;
				case ResearcheType.Name:
					if (objectList [a].name == getSearch || approxName && (objectList [a].name.Length >= getSearch.Length && objectList [a].name.Substring (0, getSearch.Length).ToUpper ( )== getSearch.ToUpper ( )))
					{
						objTagList.Add (objectList [a]);
					}
					break;
				case ResearcheType.Layer:
					if (objectList [a].layer == int.Parse (getSearch))
					{
						objTagList.Add (objectList [a]);
					}
					break;
				case ResearcheType.Component:
					components = objectList [a].GetComponents<Component> ( );

					if (objComp == null || getAdvanceField.Length == 0)
					{
						EasySearch.EndResearch (thisPlace);

						//yield break;
					}

					for (b = 0; b < components.Length; b++)
					{
						if (components [b] != null && components [b].GetType ( )== objComp.GetType ( ))
						{
							if (checkComp)
							{
								countComp = 0;

								//if ( getAdvanceField.Length > 0 )
								//{
								getFields = components [b].GetType ( ).GetFields ( );
								for (c = 0; c < getAdvanceField.Length; c++)
								{
									for (d = 0; d < getFields.Length; d++)
									{
										if (getAdvanceField [c].GetType ( )== getFields [d].GetType ( )&& getAdvanceField [c].Name == getFields [d].Name && getAdvanceField [c].GetValue (objComp).ToString ( )== getFields [d].GetValue (components [b]).ToString ( ))
										{
											countComp++;
											break;
										}
									}
								}

								if (countComp == getAdvanceField.Length)
								{
									objTagList.Add (objectList [a]);
									break;
								}
								//}
								/*else
								{
									getProperts = components [ b ].GetType ( ).GetProperties ( );
									for ( c = 0; c < getAdvanceProp.Length; c++ )
									{
										for ( d = 0; d < getProperts.Length; d++ )
										{
											try{
												if ( getProperts [ d ].Name == getAdvanceProp [ c ].Name && getAdvanceProp [ c ].GetValue ( objComp, null ).ToString ( ) == getProperts [ d ].GetValue ( components [ b ], null ).ToString ( ) )
												{
													countComp++;
													break;
												}
											}
											catch{
												countComp++;
												break;
											}

										}
									}
									if ( countComp == getProperts.Length )
									{
										objTagList.Add ( objectList [ a ] );
										break;
									}
								}*/
							}
							else
							{
								objTagList.Add (objectList [a]);
							}
							break;
						}
					}
					break;
				case ResearcheType.MissingComp:
					components = objectList [a].GetComponents<Component> ( );

					for (b = 0; b < components.Length; b++)
					{
						if (components [b] == null)
						{
							objTagList.Add (objectList [a]);
							break;
						}
					}
					break;
				case ResearcheType.Reference:
					components = objectList [a].GetComponents<Component> ( );

					for (b = 0; b < components.Length; b++)
					{
						if (components [b] == null)
						{
							continue;
						}

						//yield return thisF;

						getFields = components [b].GetType ( ).GetFields ( );
						if (getFields.Length > 0)
						{
							foreach (var field in getFields)
							{
								try
								{
									currField = field.GetValue (components [b])as object;

									getCompName = currField.ToString ( );

									if (currField.GetType ( ).IsGenericType)
									{
										if (RefOnList (currField as System.Collections.ICollection, objComp, getPref, componentsPref))
										{
											objTagList.Add (objectList [a]);
										}
									}
									else
									{
										if (currField == objComp)
										{
											objTagList.Add (objectList [a]);
											break;
										}
										else if (getPref != null && getCompName.Length >= objComp.name.Length && getCompName.Substring (0, objComp.name.Length)== objComp.name)
										{
											checkRef = false;

											for (c = 0; c < componentsPref.Length; c++)
											{
												if (componentsPref [c].GetType ( )== currField.GetType ( ))
												{
													objTagList.Add (objectList [a]);
													checkRef = true;
													break;
												}
											}

											if (checkRef)
											{
												break;
											}
										}
										else if (getPref == null && getCompName.Length >= objComp.name.Length && getCompName.Substring (0, objComp.name.Length)== objComp.name)
										{
											objTagList.Add (objectList [a]);
										}
									}
								}
								catch
								{ }
							}
						}
						else if (getProper)
						{
							foreach (var field in components [b].GetType ( ).GetProperties ( ))
							{
								try
								{
									if (field.GetValue (components [b], null)== objComp)
									{
										objTagList.Add (objectList [a]);
										break;
									}
								}
								catch
								{ }
							}
						}
					}
					break;
				case ResearcheType.Object_Prefab:
					components = objectList [a].GetComponents<Component> ( );

					if (OtherName != "")
					{
						if (!approxName && OtherName != objectList [a].name)
						{
							continue;
						}
						else if (objectList [a].name.Length < OtherName.Length || objectList [a].name.Substring (0, OtherName.Length)!= OtherName)
						{
							continue;
						}
					}
					else if (objectList [a].name.Length < getPref.name.Length || objectList [a].name.Substring (0, getPref.name.Length)!= getPref.name)
					{
						continue;
					}

					if (samePref && GetComponentsInChildrenOfAsset (getPref).Length - GetComponentsInChildrenOfAsset (objectList [a]).Length == 0)
					{
						checkComp = true;
						if (components.Length != componentsPref.Length)
						{
							continue;
						}

						for (c = 0; c < components.Length; c++)
						{
							if (components [c] == null || componentsPref [c] == null)
							{
								if (components [c] != null || componentsPref [c] != null)
								{
									checkComp = false;
									break;
								}
							}
							else if (components [c].GetType ( )!= componentsPref [c].GetType ( ))
							{
								checkComp = false;
								break;
							}
						}

						if (!checkComp)
						{
							continue;
						}

						objTagList.Add (objectList [a]);
					}
					else if (Mathf.Abs (componentsPref.Length - components.Length)<= diffComp && Mathf.Abs (GetComponentsInChildrenOfAsset (getPref).Length - GetComponentsInChildrenOfAsset (objectList [a]).Length)<= diffChil)
					{
						objTagList.Add (objectList [a]);
					}
					break;
			}
		}

		if (objTagList.Count > 0)
		{
			ReOrderObj (objTagList, CurrList);
		}

		//yield return thisF;
	}

	static void ReOrderObj (List<GameObject> listSearch, List<ObjectParent> ObjAndParent)
	{
		List<GameObject> CurrList;
		List<GameObject> addToList;
		GameObject currIO;
		Transform currParent;
		Transform checkParent;

		bool checkOAP;

		int a = 0;
		int b;
		int c;
		int countOAP;

		for (a = 0; a < listSearch.Count; a++)
		{
			checkOAP = false;
			currIO = listSearch [a];

			if (listSearch [a].transform.parent != null)
			{
				currParent = listSearch [a].transform.parent;
				for (b = 0; b < ObjAndParent.Count; b++)
				{
					if (currParent == ObjAndParent [b].CurrParent)
					{
						ObjAndParent [b].AllObj.Add (currIO);

						checkOAP = true;
						break;
					}
				}
			}
			else
			{
				currParent = listSearch [a].transform;
			}

			if (!checkOAP)
			{
				countOAP = ObjAndParent.Count;

				ObjAndParent.Add (new ObjectParent ( ));
				ObjAndParent [countOAP].AllObj = new List<GameObject> ( );
				ObjAndParent [countOAP].CurrParent = currParent;
				ObjAndParent [countOAP].AllObj.Add (currIO);
			}
		}

		for (a = 0; a < ObjAndParent.Count; a++)
		{
			checkParent = ObjAndParent [a].CurrParent;

			while (checkParent.parent != null)
			{
				checkParent = checkParent.parent;
			}

			ObjAndParent [a].CurrParent = checkParent;
		}

		for (a = 0; a < ObjAndParent.Count; a++)
		{
			for (b = 0; b < ObjAndParent.Count; b++)
			{
				if (b == a)
				{
					continue;
				}

				checkParent = ObjAndParent [a].CurrParent;
				currParent = ObjAndParent [b].CurrParent;

				if (currParent == checkParent)
				{
					CurrList = ObjAndParent [b].AllObj;
					addToList = ObjAndParent [a].AllObj;

					for (c = 0; c < CurrList.Count; c++)
					{
						addToList.Add (CurrList [c]);
					}

					ObjAndParent.RemoveAt (b);
					b--;

					if (a > b)
					{
						a--;
					}
				}
			}
		}
	}

	public static GameObject [ ] GetComponentsInChildrenOfAsset (GameObject go)
	{
		List<GameObject> tfs = new List<GameObject> ( );

		CollectChildren (tfs, go.transform);
		return tfs.ToArray ( );
	}

	static void CollectChildren (List<GameObject> transforms, Transform tf)
	{
		transforms.Add (tf.gameObject);

		foreach (Transform child in tf)
		{
			CollectChildren (transforms, child);
		}
	}

	static bool RefOnList (System.Collections.ICollection thisList, Object thisOjb, GameObject getPref, Component [ ] componentsPref)
	{
		object thisVal;
		string thisName;
		int a;

		foreach (var thisValue in thisList)
		{
			foreach (var field in thisValue.GetType ( ).GetFields ( ))
			{
				try
				{
					thisVal = field.GetValue (thisValue)as object;

					thisName = thisVal.ToString ( );

					if (thisVal.GetType ( ).IsGenericType)
					{
						if (RefOnList (thisVal as System.Collections.ICollection, thisOjb, getPref, componentsPref))
						{
							return true;
						}
					}
					else
					{
						if (thisVal == thisOjb)
						{
							return true;
						}
						else if (getPref != null && thisName.Length >= thisOjb.name.Length && thisName.Substring (0, thisOjb.name.Length)== thisOjb.name)
						{
							for (a = 0; a < componentsPref.Length; a++)
							{
								if (componentsPref [a].GetType ( )== thisVal.GetType ( ))
								{
									return true;
								}
							}
						}
						else if (getPref == null && thisName.Length >= thisOjb.name.Length && thisName.Substring (0, thisOjb.name.Length)== thisOjb.name)
						{
							return true;
						}
					}
				}
				catch
				{ }
			}
		}

		return false;
	}
	#endregion
}

public class EnumByType
{
	public IEnumerator ThisEnum;
	public TypePlace ThisPlace;
}