﻿using UnityEngine;

public struct AudioToken
{
	#region Variables
	public Transform OnThisPos;
	public AudioType ThisType;
	public AudioAction AudioAct;
	public System.Action ThisAct;
	public System.Action MidiActSpawn;
	public string ThisName;
	public string CategorieName;

	public bool LoopAudio;
	public bool CustomPath;
	public float WaitStart;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes
	#endregion
}