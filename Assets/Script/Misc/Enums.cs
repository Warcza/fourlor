﻿#region Type
public enum TokenType
{
	Deactivate,
	OpenAudio,
	Transition,
	GameOver,
	Menu,
	PlayMode
}

public enum RandomType
{
	Standard,
	Clamp,
	Nothing
}

public enum MenuType
{
	None,
	Transition,
	Home,
	Mode,
	Over,
	Score,
	Play,
	Credit,
	Bonus
}

public enum AudioType
{
	MBG,
	Sound,
	FX,
	Other
}

public enum GameType
{
	Standard,
	GodMode,
	BlackWhith,
	Nothing
}
#endregion

public enum AudioAction
{
	Nothing,
	Renew,
	PlayIfEmpty
}

public enum TargetDest
{
	Random,
	BorderRandom,
	Mid,
	Top,
	Bot,
	Right,
	Left
}

public enum SpawnPos
{
	Random,
	BorderManus,
	BorderPlus,
	Top,
	Bot,
	Right,
	Left
}