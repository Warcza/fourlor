﻿using UnityEngine;

public class PlayModeToken : MenuTokenAbstract
{
	#region Variables
	public override TokenType TokenT
	{
		get
		{
			return TokenType.PlayMode;
		}
	}

	public GameType ThisMode;
	public AudioClip ThisClip;

	public string CategorieName;
	public string MusicName;
	public byte [ ] data;
	public int Level;
	public bool CustomMusique;
	public bool CustomPath;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes

	#endregion
}