﻿using UnityEngine;

public class GameOverToken : MenuTokenAbstract
{
	#region Variables
	public override TokenType TokenT
	{
		get
		{
			return TokenType.GameOver;
		}
	}

	public int [ ] AllScore;
	public PlayModeToken playToke;
	public bool Perfect;
	public bool searchData;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes

	#endregion
}