﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "DetailScript", menuName = "Scriptable/Detail")]
public class DetailScriptable : ScriptableObject 
{
	#region Variables
	public AllDetail[] AllDetail;
	
	public GameType ThisType;
	[HideInInspector] public int CurrIndex;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	#endregion

	#region Private Methodes
	#endregion
}

[System.Serializable]
public struct AllDetail
{
	public string CategorieName;
	public string AudioName;
	public int Lvl;

	public int Score;
	public bool Perfect;
	public bool UnLock;
}
