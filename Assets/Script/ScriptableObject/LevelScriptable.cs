﻿using UnityEngine;

[CreateAssetMenu (fileName = "LevelScript", menuName = "Scriptable/LevelScript")]
public class LevelScriptable : ScriptableObject
{
	#region Variables
	public LevelInfo ThisLevel;
	//public MusicInfo[] AllLvl;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	#endregion

	#region Private Methodes
	#endregion
}

[System.Serializable]
public struct LevelInfo
{
	public int LvlScript;
	public GameType ThisType;
	public LvlInfo InfoLevel;
	public int [ ] LvlList;

	[HideInInspector] public string [ ] CategorieName;
}

[System.Serializable]
public struct LvlInfo
{
	public int WaveLvlUP;
	public float TimeToDestination;
	[Range (2, 4)] public int NbrSpawner;

	public bool SameLineShortSpace;
	public int MaxSameLine;
	public float Dist;
}