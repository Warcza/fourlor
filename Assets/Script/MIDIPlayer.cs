﻿#if UNITY_EDITOR || UNITY_STANDALONE
using System.Collections.Generic;
#endif
using System.Threading;
using System.Threading.Tasks;

#if UNITY_EDITOR || UNITY_STANDALONE
using CSharpSynth.Midi;
using CSharpSynth.Sequencer;
using CSharpSynth.Synthesis;
#endif

using UnityEngine;

public class MIDIPlayer : MonoBehaviour
{
    float currTimeLoaded = 0;
    //Try also: "FM Bank/fm" or "Analog Bank/analog" for some different sounds
#if UNITY_EDITOR || UNITY_STANDALONE
    [Space (10)]
    public AudioScriptable thisAudio;
    public string AudioName;
    [Space (10)]

    public List<int> allRecord;

    [Space (10)]
    [SerializeField] int bufferSize = 1024;
    [SerializeField] bool useEvent = false;

    float [ ] sampleBuffer;

    MidiSequencer midiSequencer;
    StreamSynthesizer midiStreamSynthesizer;

    float currTime;
    System.Action thisAction;
#endif

    CancellationTokenSource thisTok;

    // Awake is called when the script instance
    // is being loaded.
    void Awake ( )
    {
        thisTok = new CancellationTokenSource ( );

#if UNITY_EDITOR || UNITY_STANDALONE

        midiStreamSynthesizer = new StreamSynthesizer (44100, 2, bufferSize);
        sampleBuffer = new float [midiStreamSynthesizer.BufferSize];

        midiSequencer = new MidiSequencer (midiStreamSynthesizer);
        midiSequencer.Looping = false;
        //These will be fired by the midiSequencer when a song plays. Check the console for messages if you uncomment these
        if (useEvent)
        {
            midiSequencer.NoteOnEvent += new MidiSequencer.NoteOnEventHandler (MidiNoteOnHandler);
        }

        allRecord = new List<int> ( );
#endif

        //midiSequencer.NoteOffEvent += new MidiSequencer.NoteOffEventHandler (MidiNoteOffHandler);			
    }

    void Update ( )
    {
        currTimeLoaded = Time.timeSinceLevelLoad;
    }

#if UNITY_EDITOR || UNITY_STANDALONE
    public void MidiNoteOnHandler ( )
    {
        if (thisAction != null)
        {
            thisAction.Invoke ( );
        }

        allRecord.Add ((int)((currTimeLoaded - currTime) * 1000));
        currTime = currTimeLoaded;
    }

    public void LoadSong (string midiPath, uint bpm, System.Action setAction = null)
    {
        try
        {
            if (thisTok != null)
            {
                if (thisTok.Token.CanBeCanceled)
                {
                    thisTok.Cancel ( );
                }
                thisTok.Dispose ( );
            }
        }
        catch
        {

        }

        thisTok = new CancellationTokenSource ( );

        allRecord.Clear ( );
        currTime = Time.timeSinceLevelLoad;
        thisAction = setAction;
#if UNITY_EDITOR || UNITY_STANDALONE
        midiSequencer.LoadMidi (midiPath, bpm, false);
        midiSequencer.Play ( );
#endif
    }

    public void LoadSong (byte [ ] midiPath, uint bpm, System.Action setAction = null)
    {
        if (thisTok != null)
        {
            if (thisTok.Token.CanBeCanceled)
            {
                thisTok.Cancel ( );
            }
            thisTok.Dispose ( );
        }
        thisTok = new CancellationTokenSource ( );
        allRecord.Clear ( );
        currTime = Time.timeSinceLevelLoad;
        thisAction = setAction;
#if UNITY_EDITOR || UNITY_STANDALONE
        midiSequencer.LoadMidi (midiPath, bpm, false);
        midiSequencer.Play ( );
#endif
    }

#endif
    public void StopSong ( )
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        midiSequencer.Stop (true);
#endif

        if (thisTok != null)
        {
            if (thisTok.Token.CanBeCanceled)
            {
                thisTok.Cancel ( );
            }

            thisTok.Dispose ( );
        }
    }

    public async void AsyncLoadSong (int [ ] allSpawn, System.Action midiAct)
    {
        if (thisTok != null)
        {
            thisTok.Cancel ( );
            thisTok.Dispose ( );
        }
        thisTok = new CancellationTokenSource ( );
        CancellationToken newTok = thisTok.Token;

        try
        {
            int length = allSpawn.Length;
            for (int a = 0; a < length; a++)
            {
                await Task.Delay (allSpawn [a], newTok);
                if (midiAct != null)
                {
                    midiAct.Invoke ( );
                }
            }
        }
        catch
        {

        }

    }
    // See http://unity3d.com/support/documentation/ScriptReference/MonoBehaviour.OnAudioFilterRead.html for reference code
    //	If OnAudioFilterRead is implemented, Unity will insert a custom filter into the audio DSP chain.
    //
    //	The filter is inserted in the same order as the MonoBehaviour script is shown in the inspector. 	
    //	OnAudioFilterRead is called everytime a chunk of audio is routed thru the filter (this happens frequently, every ~20ms depending on the samplerate and platform). 
    //	The audio data is an array of floats ranging from [-1.0f;1.0f] and contains audio from the previous filter in the chain or the AudioClip on the AudioSource. 
    //	If this is the first filter in the chain and a clip isn't attached to the audio source this filter will be 'played'. 
    //	That way you can use the filter as the audio clip, procedurally generating audio.
    //
    //	If OnAudioFilterRead is implemented a VU meter will show up in the inspector showing the outgoing samples level. 
    //	The process time of the filter is also measured and the spent milliseconds will show up next to the VU Meter 
    //	(it turns red if the filter is taking up too much time, so the mixer will starv audio data). 
    //	Also note, that OnAudioFilterRead is called on a different thread from the main thread (namely the audio thread) 
    //	so calling into many Unity functions from this function is not allowed ( a warning will show up ). 	
#if UNITY_EDITOR || UNITY_STANDALONE
    void OnAudioFilterRead (float [ ] data, int channels)
    {
        //This uses the Unity specific float method we added to get the buffer
        midiStreamSynthesizer.GetNext ( );
    }
#endif
}