﻿using Unity.Collections;
/*using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;*/

using UnityEngine;
using UnityEngine.Jobs;
public class RotateCube : MonoBehaviour
{
	#region Variables
	[SerializeField] Transform ParentSpawn;
	[SerializeField] Transform ObjSpawn;
	//[SerializeField] Mesh MeshObjSpawn;
	//[SerializeField] Material MaterialObjSpawn;
	[SerializeField] int NbrSpawn;
	[SerializeField] RandomType Rand;

	Transform [ ] TransTarget;

	Vector3 MaxFollow;
	Vector3 MinFollow;

	[SerializeField] Vector3 MaxDist;
	[SerializeField] Vector3 MinDist;

	[SerializeField] Vector3 MaxMoveSpeed;
	[SerializeField] Vector3 MinMoveSpeed;

	public NativeArray<Vector3> ValueFollow;
	public NativeArray<Vector3> DistFollow;
	public NativeArray<Vector3> MoveSpeed;
	//public NativeArray<Position> Position;

	public TransformAccessArray TransArray;

	//NativeArray<Entity> instances;

	//Component [ ] allComp;
	public Transform target;
	#endregion

	#region Mono
	void Start ( )
	{
		/*EntityManager entityManager = World.Active.GetOrCreateManager<EntityManager> ( );
		Entity thisEntity = entityManager.CreateEntity (
			ComponentType.Create<TransformMatrix> ( ),
			ComponentType.Create<Rotation> ( ),
			ComponentType.Create<Position> ( )
		);

		instances = new NativeArray<Entity> (NbrSpawn, Allocator.Persistent);
		entityManager.Instantiate (thisEntity, instances);

		MinFollow = Vector3.one * -5f;
		MaxFollow = Vector3.one * 5f;

		ValueFollow = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);
		DistFollow = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);
		Position = new NativeArray<Position> (NbrSpawn, Allocator.Persistent);
		MoveSpeed = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);*/

		TransTarget = new Transform [NbrSpawn];
		for (int a = 0; a < NbrSpawn; a++)
		{
			//Position [a] = entityManager.GetComponentData<Position> (instances [a]);

			TransTarget [a] = (Transform)Instantiate (ObjSpawn, target.position, Quaternion.identity, ParentSpawn);
			TransTarget [a].name = a.ToString ( );
		}

		MinFollow = Vector3.one * -5f;
		MaxFollow = Vector3.one * 5f;

		TransArray = new TransformAccessArray (TransTarget);
		
		ValueFollow = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);
		DistFollow = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);
		MoveSpeed = new NativeArray<Vector3> (NbrSpawn, Allocator.Persistent);

		ValueFollow.CopyFrom (Manager.Job.IniVectorArray (MinFollow, MaxFollow, RandomType.Standard, NbrSpawn));
		DistFollow.CopyFrom (Manager.Job.IniVectorArray (MinDist, MaxDist, Rand, NbrSpawn));
		MoveSpeed.CopyFrom (Manager.Job.IniVectorArray (MinMoveSpeed, MaxMoveSpeed, Rand, NbrSpawn));

		Manager.Job.DisposeVector3 ( );
	}

	#endregion

	#region Public Methodes
	#endregion

	#region Private Methodes
	void OnApplicationQuit ( )
	{
		//Position.Dispose ( );
		//instances.Dispose ( );
		ValueFollow.Dispose ( );
		DistFollow.Dispose ( );
		MoveSpeed.Dispose ( );
	}
	#endregion

	#region Private C#Jobs

	#endregion
}