﻿using UnityEngine;

public class ObsData : MonoBehaviour
{
	public LineController Parent;
	public Vector3 Destination;
	public Vector3 Direction;

	public float MoveSpeed = 0;
	public float SizeTarget = 100;

	public bool CheckDetect = false;
	public bool IsActive = false;

	public void CalculMoveSpeed (float timeDest, float totalTravel, Vector3 pos, Vector3 target)
	{
		ResetValue ( );

		MoveSpeed = totalTravel / timeDest;
		Direction = -pos.normalized;
		Destination = target;

		IsActive = true;
	}

	public void ResetValue ( )
	{
		CheckDetect = false;
		IsActive = false;
	}
}