﻿using UnityEngine;

public class LineController : MonoBehaviour
{
	#region Variables
	[HideInInspector] public GameObject PrefabObj;
	[HideInInspector] public Vector3 pos;
	[HideInInspector] public int Line;

	[SerializeField] Transform SpawnPos;
	[SerializeField] Transform TargetPos;

	[SerializeField] Transform ParentGarbage;

	Vector3 posTarget;

	float dist;
	int nbrObj = 0;
	#endregion

	#region Mono
	void Awake ( )
	{
		pos = SpawnPos.localPosition;
		posTarget = TargetPos.localPosition;
		dist = Vector3.Distance (pos, posTarget);
	}
	#endregion

	#region Public Methodes
	public void SpawnThisObj (ObsData thisObj, float timeDest)
	{
		thisObj.Parent = this;
		thisObj.transform.SetParent (ParentGarbage);
		thisObj.transform.localPosition = pos;
		thisObj.CalculMoveSpeed (timeDest, dist, pos, posTarget);

		nbrObj++;
	}

	public void RemoveObj ( )
	{
		nbrObj--;
	}

	public void CleanAll ( )
	{
		nbrObj--;
	}
	#endregion

	#region Private Methodes

	#endregion
}