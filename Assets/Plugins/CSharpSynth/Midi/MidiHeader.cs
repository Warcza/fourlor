﻿namespace CSharpSynth.Midi
{
    public class MidiHeader
    {
        //--Variables
        public int DeltaTiming;
        public MidiHelper.MidiFormat MidiFormat;
        //--Public Methods
        public MidiHeader()
        {

        }
    }
}
