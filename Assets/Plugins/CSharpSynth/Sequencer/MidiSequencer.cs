﻿using System;
using System.Collections.Generic;

using CSharpSynth.Midi;
using CSharpSynth.Synthesis;

using UnityEngine;

namespace CSharpSynth.Sequencer
{
    public class MidiSequencer
    {
        //--Variables
        MidiFile _MidiFile;
        StreamSynthesizer synth;
        MidiSequencerEvent seqEvt;

        double PitchWheelSemitoneRange = 2.0;
        bool playing = false;
        bool looping = false;
        int sampleTime;
        int eventIndex;

        //--Events
        public delegate void NoteOnEventHandler ( );
        public event NoteOnEventHandler NoteOnEvent;

        //--Public Properties
        public bool isPlaying
        {
            get
            {
                return playing;
            }
        }
        public int SampleTime
        {
            get
            {
                return sampleTime;
            }
        }
        public int EndSampleTime
        {
            get
            {
                return (int)_MidiFile.Tracks.TotalTime;
            }
        }

        public double PitchWheelRange
        {
            get
            {
                return PitchWheelSemitoneRange;
            }
            set
            {
                PitchWheelSemitoneRange = value;
            }
        }
        //--Public Methods
        public MidiSequencer (StreamSynthesizer synth)
        {
            this.synth = synth;
            this.synth.setSequencer (this);
            seqEvt = new MidiSequencerEvent ( );
        }

        public bool Looping
        {
            get
            {
                return looping;
            }
            set
            {
                looping = value;
            }
        }
        public bool LoadMidi (MidiFile midi, bool UnloadUnusedInstruments, uint bpm)
        {
            if (playing == true)
                return false;
            _MidiFile = midi;
            if (_MidiFile.SequencerReady == false)
            {
                try
                {
                    //Convert delta time to sample time
                    eventIndex = 0;
                    uint lastSample = 0;
                    int length = _MidiFile.Tracks.MidiEvents.Length;

                    for (int x = 0; x < length; x++)
                    {
                        _MidiFile.Tracks.MidiEvents [x].deltaTime = lastSample + (uint)DeltaTimetoSamples (_MidiFile.Tracks.MidiEvents [x].deltaTime);
                        lastSample = _MidiFile.Tracks.MidiEvents [x].deltaTime;
                    }

                    //Set total time to proper value
                    _MidiFile.Tracks.TotalTime = _MidiFile.Tracks.MidiEvents [_MidiFile.Tracks.MidiEvents.Length - 1].deltaTime;
                    //reset tempo
                    _MidiFile.BeatsPerMinute = 120;
                    //mark midi as ready for sequencing
                    _MidiFile.SequencerReady = true;
                }
                catch (Exception ex)
                {
                    //UnitySynth
                    Debug.Log ("Error Loading Midi:\n" + ex.Message);
                    return false;
                }
            }

            return true;
        }
        public bool LoadMidi (string file, uint bpm, bool UnloadUnusedInstruments)
        {
            if (playing == true)
                return false;

            if (_MidiFile == null)
            {
                Debug.Log (file);
                try
                {
                    _MidiFile = new MidiFile (file, bpm);
                }
                catch (Exception ex)
                {
                    //UnitySynth
                    Debug.Log ("Error Loading Midi:\n" + ex.Message);
                    return false;
                }
            }
            else
            {
                _MidiFile.resetMidi (file, bpm);
                _MidiFile.SequencerReady = false;
            }

            return LoadMidi (_MidiFile, UnloadUnusedInstruments, bpm);
        }

        public bool LoadMidi (byte [ ] file, uint bpm, bool UnloadUnusedInstruments)
        {
            if (playing == true)
                return false;

            if (_MidiFile == null)
            {
                Debug.Log (file);
                try
                {
                    _MidiFile = new MidiFile (file, bpm);
                }
                catch (Exception ex)
                {
                    //UnitySynth
                    Debug.Log ("Error Loading Midi:\n" + ex.Message);
                    return false;
                }
            }
            else
            {
                _MidiFile.resetMidi (file, bpm);
                _MidiFile.SequencerReady = false;
            }

            return LoadMidi (_MidiFile, UnloadUnusedInstruments, bpm);
        }

        public void Play ( )
        {
            if (playing == true)
                return;
            //Clear the current programs for the channels.
            //Clear vol, pan, and tune
            //set bpm
            _MidiFile.BeatsPerMinute = 120;
            //Let the synth know that the sequencer is ready.
            eventIndex = 0;
            playing = true;
        }
        public void Stop (bool immediate)
        {
            playing = false;
            sampleTime = 0;
        }

        public MidiSequencerEvent Process (int frame)
        {
            seqEvt.Events.Clear ( );
            //stop or loop
            if (sampleTime >= (int)_MidiFile.Tracks.TotalTime)
            {
                sampleTime = 0;
                if (looping == true)
                {
                    //Clear the current programs for the channels.
                    _MidiFile.BeatsPerMinute = 120;
                    //Let the synth know that the sequencer is ready.
                    eventIndex = 0;
                }
                else
                {
                    playing = false;
                    return null;
                }
            }
            while (eventIndex < _MidiFile.Tracks.EventCount && _MidiFile.Tracks.MidiEvents [eventIndex].deltaTime < (sampleTime + frame))
            {
                seqEvt.Events.Add (_MidiFile.Tracks.MidiEvents [eventIndex]);
                eventIndex++;
            }
            return seqEvt;
        }

        int checkMalus = 5;
        public void IncrementSampleCounter (int amount)
        {
            checkMalus--;
            if (checkMalus == 0)
            {
                checkMalus = 5;
                sampleTime += 3;
            }
            sampleTime = sampleTime + amount;
        }

        public void ProcessMidiEvent (MidiEvent midiEvent)
        {
            if (midiEvent.midiChannelEvent != MidiHelper.MidiChannelEvent.None)
            {
                if (midiEvent.midiChannelEvent == MidiHelper.MidiChannelEvent.Note_On && this.NoteOnEvent != null)
                {
                    this.NoteOnEvent ( );
                }
            }
        }

        public void Dispose ( )
        {
            Stop (true);
            //Set anything that may become a circular reference to null...
            synth = null;
            _MidiFile = null;
            seqEvt = null;
        }
        //--Private Methods
        private int DeltaTimetoSamples (uint DeltaTime)
        {
            return SynthHelper.getSampleFromTime (synth.SampleRate, (DeltaTime * (60.0f / (((int)_MidiFile.BeatsPerMinute) * _MidiFile.MidiHeader.DeltaTiming))));
        }
    }
}