﻿using System;

namespace CSharpSynth.Synthesis
{
    public static class SynthHelper
    {
        public static int getSampleFromTime(int sampleRate, float timeInSeconds)
        {
            return (int)(sampleRate * timeInSeconds);
        }
        public static float getTimeFromSample(int sampleRate, int Sample)
        {
            return Sample / (float)sampleRate;
        }
    }
}
