﻿using System;
using System.Collections.Generic;
using CSharpSynth.Sequencer;
using UnityEngine;
using System.Threading;

namespace CSharpSynth.Synthesis
{
    public class StreamSynthesizer
    {
        //--Variables
        private float[,] sampleBuffer;
        private int rawBufferLength;
        private MidiSequencer seq;
        //Set "once parameters"
        private int audioChannels = 1;
        private int sampleRate = 44100;
        private int samplesperBuffer = 2000;
        //--Public Properties
        public int BufferSize
        {
            get { return rawBufferLength; }
        }
      
        public int SampleRate
        {
            get { return sampleRate; }
        }
  
        public StreamSynthesizer(int sampleRate, int audioChannels, int bufferSize)
        {
            this.sampleRate = sampleRate;
            this.audioChannels = audioChannels;
            //UnitySynth
            this.samplesperBuffer = bufferSize;
            setupSynth();
        }
     
        public void setSequencer(MidiSequencer sequencer)
        {
            this.seq = sequencer;
        }
      
        public void GetNext()
        {//Call this to process the next part of audio and return it in raw form.
            ClearWorkingBuffer();
            FillWorkingBuffer();
        }


        private void FillWorkingBuffer()
        {
            // Call Process on all active voices
            if (seq != null && seq.isPlaying)//Use sequencer
            {
                MidiSequencerEvent seqEvent = seq.Process(samplesperBuffer);
                if (seqEvent == null)
                    return;
                int oldtime = 0;
                int waitTime = 0;
                for (int x = 0; x < seqEvent.Events.Count; x++)
                {
                    waitTime = ((int)seqEvent.Events[x].deltaTime - seq.SampleTime) - oldtime;
                    oldtime = oldtime + waitTime;
                    //Now process the event
                    seq.ProcessMidiEvent(seqEvent.Events[x]);
                }
                //make sure to finish the processing to the end of the buffer
           
                //increment our sample count
                seq.IncrementSampleCounter(samplesperBuffer);
            }
        }
        private void ClearWorkingBuffer()
        {
            Array.Clear(sampleBuffer, 0, audioChannels * samplesperBuffer);
        }
        private void setupSynth()
        {
            //checks
            if (sampleRate < 8000 || sampleRate > 48000)
            {
                sampleRate = 44100;
                this.samplesperBuffer = (sampleRate / 1000) * 50;
                //UnitySynth
                Debug.Log("-----> Invalid Sample Rate! Changed to---->" + sampleRate);
                Debug.Log("-----> Invalid Buffer Size! Changed to---->" + 50 + "ms");
            }
  
            if (samplesperBuffer < 100 || samplesperBuffer > 500000)
            {
                this.samplesperBuffer = (int)((sampleRate / 1000.0) * 50.0);
                Debug.Log("-----> Invalid Buffer Size! Changed to---->" + 50 + "ms");
            }
            if (audioChannels < 1 || audioChannels > 2)
            {
                audioChannels = 1;
                Debug.Log("-----> Invalid Audio Channels! Changed to---->" + audioChannels);
            }
            //initialize variables
            sampleBuffer = new float[audioChannels, samplesperBuffer];
            rawBufferLength = audioChannels * samplesperBuffer * 2; //Assuming 16 bit data
            // Create voice structures
            //Setup Channel Data
        }
    }
}
